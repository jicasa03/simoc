<?php   
  $nombres     = '';
  $foto_perfil = '';
?>


<!DOCTYPE html>
<html class="loading" lang="es" data-textdirection="ltr">
<?php include('Layout/css.php'); ?>

<style type="text/css">
  

</style>


  <body id="cuerpo" class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar menu-expanded" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">














    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav flex-row">
            <li class="nav-item mobile-menu d-lg-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo base_url() ?>Control"><img class="brand-logo" alt="modern admin logo" src="<?php echo base_url() ?>public/app-assets/images/logo/logo.png">
                <h3 class="brand-text">SIMOC</h3></a></li>
            <li class="nav-item d-none d-lg-block nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="toggle-icon ft-toggle-right font-medium-3 white" data-ticon="ft-toggle-right"></i></a></li>
            <li class="nav-item d-lg-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content">
          <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
              <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
        
            </ul>
            <ul class="nav navbar-nav float-right">
              <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i><span class="badge badge-pill badge-danger badge-up badge-glow" id="notican"></span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <li class="dropdown-menu-header">
                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span></h6><span class="notification-tag badge badge-danger float-right m-0" id="notican"> </span>
                  </li>
                  <li class="scrollable-container media-list w-100"><a href="javascript:void(0)"></a>

                                      <li id="notificacion" class="scrollable-container media-list w-100">
                                   


                                 <!--      <a href="javascript:void(0)">
                                        <div class="media">
                                          <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                                          <div class="media-body">
                                            <h6 class="media-heading">You have new order!</h6>
                                            <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p><small>
                                              <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">30 minutes ago</time></small>
                                          </div>
                                        </div></a>-->

                  </li>
                </li>
                  <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="javascript:void(0)">Leer todas Notificaciones</a></li>
                </ul>
              </li>
              <li class="dropdown dropdown-user nav-item">
                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                  <span class="mr-1 user-name text-bold-700"><?php echo $_COOKIE ["usuario"]; ?></span>
                  <span class="avatar avatar-online"><img src="<?php echo base_url() ?>public/app-assets/images/portrait/small/avatar-s-19.png" alt="avatar"><i></i></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="user-profile.html">
                        <i class="ft-user"></i> Editar Perfil
                    </a>
                  <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url() ?>Login/cerrar_session"><i class="ft-power"></i> Cerrar Sesión</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>


    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          <li class=" navigation-header"><span data-i18n="nav.category.pages">Modulos</span>
          </li> 
          <?php foreach ($lista as $value) {  
            if(count($value["modulos"])>0){ ?> 
              <li class=" nav-item"><a href="#"><i class="<?php echo $value["mod_icono"] ?>"></i><span class="menu-title" data-i18n="nav.invoice.main"><?php echo $value["mod_descripcion"]?></span></a>
                <ul class="menu-content">
                  <?php foreach ($value["modulos"] as $val) { ?>
                    <li id="<?php  echo $val['mod_url'];?>"><a class="menu-item" href="<?php echo base_url().$val['mod_url']; ?>"><i></i><span data-i18n="nav.invoice.invoice_summary"><?php echo $val["mod_descripcion"]?></span></a>
                    </li>
                  <?php } ?>
                </ul>
              </li> 
            <?php }} ?>

        </ul>
      </div>
    </div>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row mb-1">
          <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block"><?php echo $data["titulo_descripcion"] ?></h3>
          </div>
        </div>
        <div class="content-body"><!-- Description -->


          

