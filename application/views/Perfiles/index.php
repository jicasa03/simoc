 <!-----------------------------footer-------------------------------- -->
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title"></h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      <div class="card-text">





<div class="grid simple ">
<div class="grid-title no-border">

<!-----------------------------footer-------------------------------- -->

<div class="grid-body no-border"><a href="<?php echo base_url() ?>Perfiles/nuevo"><button id="test2" class="btn btn-primary">NUEVO PERFIL</button></a>
<br>
<br>
<div class="row">
	<div class="col-md-12">
		<table class="table" id="example3">
			<thead>
			<tr>
				<th>#</th>
				<th>Nombre Perfil</th>
				<th>Observacion</th>

				<th>Acción</th>
			</tr>
			</thead>
			<tbody>
                 <?php
				 foreach ($data["lista"] as $key => $value){
                    $a=$value["per_id"].",'".$value["per_descripcion"]."','".$value["observacion"]."'";
				 	echo "<tr>";
				 	echo "<td>".($key+1)."</td>";
					 echo "<td>".$value["per_descripcion"]."</td>";
					 echo "<td>".$value["observacion"]."</td>";
					 echo '<td class="center">
                     <a title="Dar Permisos" class="btn btn-success" onclick="mostrar('.$value["per_id"].')">
<i class="la la-eye " style="color:white"></i></a>



<a title="Editar" href="'.base_url().'Perfiles/editar/'.$value["per_id"].'" class="btn btn-primary"><i style="color:white" class="la la-paste "></i> </a>




<a title="Eliminar" class="btn btn-danger" onclick="eliminar('.$value["per_id"].')">
<i class="la la-trash " style="color:white"></i></a></td>';
				 	echo "</tr>";

				 }

				 ?>
			</tbody>
		</table>
	</div>
</div>






<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
                    <form id="form_permiso" name="form_permiso" onsubmit="return guardar_permiso()">  
            <div class="modal-header">
           
            <br>
           
            <h4 id="myModalLabel" class="semi-bold">ASIGNAR PERMISOS POR MÓDULOS</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <br>
            </div>
            <div class="modal-body">
        <input type="hidden" name="id_permiso" value="" id="id_permiso">
            
      

                <?php


$lista_modulos =$data["modulo"];
                 foreach ($lista_modulos as $key => $value) {
              if(count($value["lista"])>0){ 
                 ?>
                    <div class="row">
                    <h4 style="padding-left: 20px;"><?php echo $value["mod_descripcion"] ?></h4>
                        </div>    
                        <div class="row">

                            <?php foreach ($value["lista"]  as $key1 => $value1) {
                                # code...
                         ?>
                            <div class="col-md-3">
                                 <div class="row-fluid">
                                    <div class="checkbox check-primary">
                                    <input id="checkbox_<?php echo $value1["mod_id"]; ?>" name="permiso[]" type="checkbox" value="<?php echo $value1["mod_id"]; ?>">
                                    <label for="checkbox_<?php echo $value1["mod_id"]; ?>"><?php echo $value1["mod_descripcion"]; ?></label>
                                    </div>
                            </div>  
                            </div>
                            <?php } ?>
                             
                </div>

            <?php


            }
             } ?>

      
            </div>
             <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary" id="boton_permiso">Guardar Cambio</button>
            </div>

               </form>
              
        </div>

        </div>

</div>

</div>





<!-----------------------------footer-------------------------------- -->
</div>
</div>


      </div>
    </div>
  </div>
</section>  


<!-----------------------------footer-------------------------------- -->


<script>















function guardar_permiso(){

         $("#boton_permiso").attr("disable",true);
         $("#boton_permiso").text("Guardando...");

         $.post(base_url+"Perfiles/guardar_permiso",$("#form_permiso").serialize(),function(data){
                  
                
                    if(data["estado"]){
                        location.reload();
                    }else{
                          toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"]("ERROR AL REGISTRAR");

                    }
                  $("#boton_permiso").attr("disable",false);
                         $("#boton_permiso").text("Guardar Cambio");

         },"json");
    return false;
}
    function  mostrar(id) {

                     
 $('input[name="permiso[]"]').map(function () {
               //alert($(this).val());
           $(this).prop("checked",false);

             
      }).get();

        $("#id_permiso").val(id);
         $("#quick-access").css("bottom","-115px");

         $.post(base_url+"Perfiles/mostrar_permisos",{"id":id},function(data){
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                data[i];

                $("#checkbox_"+data[i]["per_modulo"]).prop("checked",true);
            }
         },"json");
         
        $("#myModal").modal();
    }

    function editar(id,descripcion,observacion){
             $('#form_perfil')[0].reset();
            $("#quick-access").css("bottom","0px");
            $("#id").val(id);
            $("#perfil").val(descripcion);
            $("#descripcion").val(observacion);
    }
    function eliminar_datos(){
        $("#modal_boton_eliminar").text("Eliminando...");
        $("#modal_boton_eliminar").attr("disable",true);
        $.post(base_url+"Perfiles/delete_perfil",{"id":$("#id_eliminar").val()},function (data) {
            console.log(data);
            if(data["estado"]){
                location.reload();
            }else{
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"]("ERROR AL REGISTRAR");


                $("#boton_agregar_modulo").text("AGREGAR MODULO");
                $("#boton_agregar_modulo").attr("disabled",false);
            }
        },"json");

    }
    function eliminar(id){
      
        $("#id_eliminar").val(id);
        $("#modal_eliminar").modal({
            keyboard: false,
            backdrop:'static',


        });


    }
    function guardar_info(){
        $("#boton_agregar_perfil").text("Guardando...");
        $("#boton_agregar_perfil").attr("disabled",true);
        $.post(base_url+"Perfiles/save_perfil",$("#form_perfil").serialize(),function(data){
            if(data["estado"]){
                location.reload();
            }
            else{
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"]("ERROR AL REGISTRAR");


                $("#boton_agregar_perfil").text("AGREGAR PERFIL	");
                $("#boton_agregar_perfil").attr("disabled",false);

            }
        },"json");
        return false;

    }
    $(document).ready(function() {
        $('#test2').on( "click",function() {

            $('#form_perfil')[0].reset();
            $("#quick-access").css("bottom","0px");
        });
        var oTable3 = $('#example3').dataTable( {
            "sDom": "<'row'<'col-md-6'l <'toolbar'>><'col-md-6'f>r>t<'row'<'col-md-12'p i>>",
            "oTableTools": {

            },
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0 ] }
            ],
            "aaSorting": [[ 3, "desc" ]],
            "oLanguage":{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });
</script>
