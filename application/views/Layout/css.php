<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
  <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
<meta name="author" content="PIXINVENT">
<title>SIMOC</title>

 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet"> 

 <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/vendors/css/vendors.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/vendors/css/ui/prism.min.css"> 
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/css/bootstrap-extended.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/css/colors.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/css/components.min.css"> 
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/css/core/menu/menu-types/vertical-menu-modern.css">


    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/css/plugins/loaders/loaders.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/css/core/colors/palette-loader.min.css">


 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/css/core/colors/palette-gradient.min.css"> 
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/assets/css/style.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/app-assets/vendors/css/tables/datatable/datatables.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/libreria/toast/toastr.min.css">

<script src="<?php echo base_url() ?>public/app-assets/vendors/js/vendors.min.js"></script> 
<script src="<?php echo base_url() ?>public/toastr.css"></script> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>public/app-assets/css/plugins/ui/jqueryui.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>public/app-assets/vendors/css/ui/jquery-ui.min.css">

      <script src="<?php echo base_url() ?>public/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
  <!--  <script src="<?php echo base_url() ?>public/app-assets/js/scripts/ui/jquery-ui/autocomplete.min.js"></script>
-->
 <script src="<?php echo base_url() ?>public/libreria/toast/toastr.min.js"></script>
<script src="<?php echo base_url() ?>public/libreria/loading/loading-bar.min.css"></script>

</head>

<script type="text/javascript">
	base_url = <?php echo base_url(); ?>
</script>