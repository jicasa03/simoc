
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/forms/toggle/switchery.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/js/scripts/forms/validation/form-validation.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

<script src="<?php echo base_url() ?>public/app-assets/vendors/js/extensions/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>

<script src="<?php echo base_url() ?>public/app-assets/vendors/js/charts/echarts/echarts.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/js/core/app-menu.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/js/core/app.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/js/scripts/customizer.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/js/scripts/footer.min.js"></script>

<script src="<?php echo base_url() ?>public/app-assets/js/scripts/pages/hospital-add-doctors.min.js"></script>
<script src="<?php echo base_url() ?>public/app-assets/js/validaciones.js"></script>
<script src="<?php echo base_url() ?>public/libreria/loading/loading-bar.min.js"></script>

<script src="<?php echo base_url() ?>public/app-assets/js/scripts/extensions/sweet-alerts.min.js"></script>
<script src="<?php echo base_url() ?>public/toastr.js"></script>




<div class="modal fade" id="modal_alerta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Alerta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="row">
           <div class="col-md-12">
             <label id="texto_alerta"></label>
           </div>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

      </div>
    </div>
  </div>
</div>




















<script type="text/javascript">


$(function(){
 // alerta_modal("se guardo correctamnete");
});


function alerta_modal(texto)
{
  $("#modal_alerta").modal();
  $("#texto_alerta").text(texto);
}






  function alerta(nombre,descripcion,url,id,id_notificacion){

     

    toastr.options = {

     

  "closeButton": true,

  "debug": false,

  "newestOnTop": false,

  "progressBar": false,

  "positionClass": "toast-bottom-right",

  "preventDuplicates": false,

  

  "showDuration": "5000",

  "hideDuration": "5000",

  "timeOut": "5000",

  "extendedTimeOut": "5000",

  "showEasing": "swing",

  "hideEasing": "linear",

  "showMethod": "fadeIn",

  "hideMethod": "fadeOut",

  //onclick: function () { alert(text in the notication); } 

   }

toastr.options.onclick = function () {

   

                        $.post(base_url+"Control/borrar_notificacion",{id:id_notificacion},function(data){

    

                       });

                     window.location=base_url+url;

                       

                };

toastr.info(descripcion,nombre);



  } 

$(function(){
   $("#<?php echo$this->router->class; ?>").addClass("active");
});

  function playSound() {



  var audio = new Audio(base_url+"/public/notificacion.mp3");

                audio.play();

} 

setInterval(function(){ 

  var data1=$("#notificacion").html(); 

  var numero=$("#notican").html();



    $.post(base_url+'Control/notificaciones1',  function(data, textStatus, xhr) {

     // alerta(data.nombre,data.nombre,data.nombre);

    if(numero!=data.cantidad || data.arp==2)

    {  

         if(data.nombre!="")

         {

             //alert("hola1");      

          $('#notificacion').empty();

          $('#notificacion').append(data.notificaciones);

          $('#notican').empty();

          $('#notican').append(data.cantidad);

           alerta(data.nombre,data.descripcion,data.url,data.id,data.id_notificacion);

          playSound();

         }

         else{

             // alert("hola2"); 

            $('#notificacion').empty();

            $('#notificacion').append(data.notificaciones);

            $('#notican').empty();

            $('#notican').append(data.cantidad);

          }

    }

  },"json");

}, 10000);

function cargar_departamento(id)
{
	var elemt=$("#"+id);
         
    $.post(base_url+"BaseController/departamento",function(response){
              elemt.empty().append(response);
               $("#"+id).val("22");
    });


}

function cargar_provincia(id,id_departamento)
{
	var elemt=$("#"+id);
         
    $.post(base_url+"BaseController/provincia/"+id_departamento,function(response){
              elemt.empty().append(response);
    });


}


function cargar_distrito(id,id_provincia)
{
	var elemt=$("#"+id);
         
    $.post(base_url+"BaseController/distrito/"+id_provincia,function(response){
              elemt.empty().append(response);
    });


}





function cargar_tipo_producto(id)
{
	var elemt=$("#"+id);
         
    $.post(base_url+"BaseController/tipo_producto",function(response){
              elemt.empty().append(response);
    });


}


function cargar_tipo_conexion(id)
{
	var elemt=$("#"+id);
         
    $.post(base_url+"BaseController/tipo_conexion",function(response){
              elemt.empty().append(response);
    });


}

















	 $('#table-table').DataTable();

	 function sololetras(e){
	key= e.keyCode || e.which;
	teclado=String.fromCharCode(key).toLowerCase();
	letras=" abcdefghijklmnñopqrstuvwxyz";
	especiales="8-37-38-46-164";
	teclado_especial=false;
	for(var i in especiales){
		if(key==especiales[i]){
			teclado_especial= true;
			break;
		}
	}
	if (letras.indexOf(teclado)==-1 && !teclado_especial){

		return false;

	}
}


function solonumeros(e){
	key=e.keyCode || e.which;
	teclado=String.fromCharCode(key);
	numeros=" 0123456789";
	especiales="8-37-38-46";
	teclado_especial=false;

	for(var i in especiales){
		if(key==especiales[i]){
			teclado_especial=true;
		}
	}
	if(numeros.indexOf(teclado)==-1 && !teclado_especial){
		return false;
	}
}


</script>






