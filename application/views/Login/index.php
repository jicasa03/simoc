<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr"> 
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
	<meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
	<title>Sistema de Almacen</title>
	<link rel="apple-touch-icon" href="<?php echo base_url();?>public/app-assets/images/ico/apple-icon-120.png">
	 
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
	<!-- BEGIN VENDOR CSS-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/vendors.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/css/forms/icheck/icheck.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/css/forms/icheck/custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/app.min.css">
	<!-- END MODERN CSS-->
	<!-- BEGIN Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/core/menu/menu-types/vertical-menu-modern.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/core/colors/palette-gradient.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/css/pages/login-register.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/app-assets/vendors/css/extensions/toastr.css">
	<!-- END Page Level CSS-->
	<!-- BEGIN Custom CSS-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/assets/css/style.css">
	<!-- END Custom CSS-->
</head>
<body class="vertical-layout vertical-menu-modern 1-column  bg-cyan bg-lighten-2 menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="1-column"
style="background-image: url(<?php echo base_url()?>public/portada/foto3.jpg)">

	<!-- fixed-top-->


	<!-- ////////////////////////////////////////////////////////////////////////////-->
	<div class="app-content content">
		<div class="content-wrapper">
			<div class="content-header row">
			</div>
			<div class="content-body"><section class="flexbox-container">
				<div class="col-12 d-flex align-items-center justify-content-center">
					<div class="col-md-4 col-10 box-shadow-2 p-0">
						<div class="card border-grey border-lighten-3 m-0">
							<div class="card-header border-0">
								<div class="card-title text-center">
									<img src="<?php echo base_url();?>public/app-assets/images/logo/logo-dark.png" alt="branding logo">
								</div>
								<h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Iniciar Sesión</span></h6>
							</div>
							<div class="card-content">
								<div class="card-body">
									<form class="form-horizontal form-material"  id="form" >
										<fieldset class="form-group position-relative has-icon-left">
											<input type="text" class="form-control input-lg" id="usuario" name="usuario" placeholder="Ingrese su Usuario" tabindex="1" required data-validation-required-message= "Por favor ingrese su usuario.">
											<div class="form-control-position">
												<i class="ft-user"></i>
											</div>
											<div class="help-block font-small-3"></div>
										</fieldset>
										<fieldset class="form-group position-relative has-icon-left">
											<input type="password" class="form-control input-lg" id="clave" name="clave" placeholder="Ingrese su Clave" tabindex="2" required data-validation-required-message= "Por favor ingrese su clave.">
											<div class="form-control-position">
												<i class="ft-unlock"></i>
											</div>
											<div class="help-block font-small-3"></div>
										</fieldset>
										<div class="form-group row">
											<div class="col-md-6 col-12 text-center text-md-left">
												<fieldset>
													<input type="checkbox" id="remember-me" class="chk-remember">
													<label for="remember-me"> Recordarme</label>
												</fieldset>
											</div>
											 
										</div>
										<button id="submit" type="button" name="submit" value="submit" class="btn btn-danger btn-block btn-lg"><i class="ft-unlock"></i> Iniciar Sesión</button>
									</form>
								</div>
							</div> 
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
 
<script src="<?php echo base_url();?>public/app-assets/vendors/js/vendors.min.js"></script>
 
<script src="<?php echo base_url();?>public/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="<?php echo base_url();?>public/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="<?php echo base_url();?>public/app-assets/js/scripts/forms/form-login-register.min.js"></script>
<script src="<?php echo base_url();?>public/app-assets/vendors/js/extensions/toastr.min.js"></script>
<script src="<?php echo base_url();?>public/app-assets/js/scripts/extensions/toastr.min.js"></script>
 
</body>

 
</html>
    <script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
    $(function() {
        $(".preloader").fadeOut();
        $('[data-toggle="tooltip"]').tooltip();
    });  
    $('#to-recover').on("click", function() {
        $("#form").slideUp();
        $("#recoverform").fadeIn();
    });
	$(document).ready(function(){
	    $("#submit").on('click', function(){    
	    	if ($("#usuario").val() == "") {
	            $("#usuario").focus();
	            return 0;
	        }
	        if ($("#clave").val() == "") {
	            $("#clave").focus();
	            return 0;
	        } 
	        llamarfuncion();
    	});
    });
	document.querySelector('#clave').addEventListener('keypress', function (e) {
	    var key = e.which || e.keyCode;
	    if (key === 13) { // 13 is enter}
	         if ($("#usuario").val() == "") {
	                    $("#usuario").focus();
	                    return 0;
	                }
	                if ($("#clave").val() == "") {
	                    $("#clave").focus();
	                    return 0;
	                }
	      llamarfuncion();
	    }
	});
	document.querySelector('#usuario').addEventListener('keypress', function (e) {
	    var key = e.which || e.keyCode;
	    if (key === 13) { // 13 is enter}
	        if ($("#usuario").val() == "") {
	            $("#usuario").focus();
	            return 0;
	        }
	        if ($("#clave").val() == "") {
	            $("#clave").focus();
	            return 0;
	        }
	      llamarfuncion();
	    }
	});
        function llamarfuncion(){
            $.ajax({
                url: base_url+'Login/Iniciar', // url where to submit the request
                type : "POST", // type of action POST || GET
                dataType : 'json', // data type
                data : { usuario : $("#usuario").val() , clave : $("#clave").val()}, // post data || get data
                success : function(result) {
                   if (result == 0) {
                   	 toastr.error("Lo sentimos el usuario o clave que ingreso es incorrecta","Usuario Incorrecto!",{positionClass:"toast-bottom-full-width",containerId:"toast-bottom-full-width"});                       
                        return 0;
                   }
                   if(result == 2){
                   	toastr.info("Lo sentimos el usuario está dado de baja","Usuario Inactivo!",{positionClass:"toast-bottom-full-width",containerId:"toast-bottom-full-width"}); 
                        return 0;
                   }
                   if (result == 1) {
                   	toastr.success("Se identifico correctamente","Bienvenido!",{positionClass:"toast-bottom-full-width",containerId:"toast-bottom-full-width"});
                     setTimeout(function(){
                     location.href = base_url+"Login"; 
                 	}, 1000); 
                   }
                },
                error: function(xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            })
        }
 

 
    </script>