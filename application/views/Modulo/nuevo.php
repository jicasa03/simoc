 <!-----------------------------footer-------------------------------- -->
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title"></h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      <div class="card-text">





<div class="grid simple ">
<div class="grid-title no-border">

<!-----------------------------footer-------------------------------- -->
		<form method="POST" id="form_modulo" onsubmit="return guardar_info()" >
			<input type="hidden"  name="mod_id" id="mod_id">
			<div class="row">
		    <div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">DESCRIPCION DEL MODULO</label>
					<input type="text" class="form-control" required="true" name="mod_descripcion" id="mod_descripcion" autofocus="true" value="">
				  </div>
				</div>
				<div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">URL</label>
					<input type="text" class="form-control" required="true" name="mod_url" id="mod_url"  value="">
				  </div>
				</div>
				<div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">ICONO</label>
					<input type="text" class="form-control" required="true" name="mod_icono" id="mod_icono" autofocus="true" value="">
				  </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label mb-10 text-left">MODULO PADRE</label>

						 <select class="form-control" name="mod_padre" id="mod_padre">
						 	<?php
						 	echo "<option value='0'>Modulo Padre</option>";

                               foreach ($data["select_modulo_padre"] as $key => $value) {
                                echo "<option value='".$value["mod_id"]."'>".$value["mod_descripcion"]."</option>";
                               }
						 	?>
						 </select>
					</div>
				</div>
			</div>
			  <br>
			  <center><a href="<?php echo  base_url();?>Modulo"><button class="btn btn-danger" type="button" >Cancelar</button><a> <button class="btn btn-primary" id="boton_agregar_modulo" onclick="guardar_info()">Guardar</button></center>
		   </form>


<!-----------------------------footer-------------------------------- -->
</div>
</div>


      </div>
    </div>
  </div>
</section>  


<!-----------------------------footer-------------------------------- -->

<script type="text/javascript">
	<?php

     if(isset($data["id"]))
     {?>


     $(function(){

     	$.post(base_url+"modulo/update_modulo",{"id":"<?php echo $data['id']?>"},function(data){
              console.log(data);
              $("#mod_id").val(data[0]["mod_id"]);
              $("#mod_descripcion").val(data[0]["mod_descripcion"]);
              $("#mod_url").val(data[0]["mod_url"]);
              $("#mod_icono").val(data[0]["mod_icono"]);

               $("#mod_padre option[value="+ data[0]["mod_padre"] +"]").attr("selected",true);


     	},"json");
     });




     <?php } ?>


     function guardar_info(){
     
     $("#boton_agregar_modulo").text("Guardando...");
$("#boton_agregar_modulo").attr("disabled",true);
     $.post(base_url+"Modulo/guardar_modulo",$("#form_modulo").serialize(),function(data){
           if(data["estado"]){
                  location.href =base_url+"Modulo";
           }
           else{
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-bottom-right",
			  "preventDuplicates": false,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
			toastr["error"]("ERROR AL REGISTRAR");


                  $("#boton_agregar_modulo").text("AGREGAR MODULO");
					$("#boton_agregar_modulo").attr("disabled",false);

           }
     },"json");
       return false;

	}
</script>