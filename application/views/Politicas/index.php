 <!-----------------------------footer-------------------------------- -->
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title"></h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      <div class="card-text">





<div class="grid simple ">
<div class="grid-title no-border">

<!-----------------------------footer-------------------------------- -->

<div class="grid-body no-border">
	<form id="formulario" onsubmit="return guardar()">
  <div class="row">
  	<div class="col-md-12">
       <div class="form-group">
       	  <label>DESCRIPCIÓN DE LA POLITICA</label>
       	  <textarea rows="5" required="true" class="form-control" id="politica" name="politica"></textarea>
       </div>
  	</div>
  	<div class="col-md-12 d-flex justify-content-center">
  		<button class="btn btn-primary" type="submit">GUARDAR POLITICA</button>
  	</div>
  </div>
</div>
</form>
	<!-----------------------------footer-------------------------------- -->
</div>
</div>


      </div>
    </div>
  </div>
</section>  


<!-----------------------------footer-------------------------------- -->


<script>
	$(function(){
		$.post(base_url+"Politicas/cargar",function(response){
             $("#politica").val(response["configuracion_politica"]);
		},"json");
	});

	function guardar()
	{
      $.post(base_url+"Politicas/guardar",$("#formulario").serialize(),function(response){
           
      },"json");

      return false;

	}
</script>

