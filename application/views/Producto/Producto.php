<section id="dom">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                 <a onclick="registrarnuevo()"><button  type="button" class="btn btn-info">+ Producto</button></a>  
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered dom-jQuery-events" id="table-table">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Tipo de producto</th>
                                        <th>Producto</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data["producto"] as $key => $value) { ?> 
                                        <tr>
                                        <td width="5%"><?php echo $value["id"] ?></td>
                                        <td width="35"><?php echo $value["tpdesc"] ?></td>
                                        <td width="35"><?php echo $value["pdesc"] ?></td>
                                        <td width="25%">
                                            <a onclick="editar('<?php echo   $value["id"] ;?>')"><button  type="button" class="btn btn-sm btn-warning box-shadow-2 mr-1">Editar</button></a> 
                                            <a onclick="eliminar('<?php echo   $value["id"] ;?>')"><button  type="button" class="btn btn-sm btn-danger box-shadow-2 mr-1">Eliminar</button></a> 
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                                    
                                    <div class="modal fade text-left" id="producto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
                                     aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header bg-info white">
                                                    <h4 class="modal-title" id="myModalLabel1"> </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                <form id="formulario"> 
                                                  <div class="row">
                                                  <div class="form-group col-md-6">
                                                    <input type="hidden" name="id" id="id" value="">
                                                  <label for="nombre">Tipo de producto</label>
                                                     <select name="tipo_prod" class="form-control">
                                                      <?php foreach ($data["tipo_producto"] as $key => $value) {?>
                                                        <option value="<?php echo ($value["id"])?>">
                                                            <?php echo $value["descripcion"]; ?></option>
                                                      <?php

                                                         } ?>
                                                    </select>
                                                  </div>
                                                  <div class="form-group col-md-6">
                                                    
                                                  <label for="nombre">Producto</label>
                                                     <input type="text" id="producto" onkeypress="return sololetras(event)" class="form-control" placeholder="Nombre" name="producto" required="required">
                                                  </div>
                                                  <div class="form-group col-md-6">
                                                
                                                  <label for="nombre">Categoria</label>
                                                     <select name="cat" class="form-control">
                                                      <?php foreach ($data["categoria"] as $key => $value) {?>
                                                        <option value="<?php echo ($value["id"])?>">
                                                            <?php echo $value["descripcion"]; ?></option>
                                                      <?php

                                                         } ?>
                                                    </select>
                                                  </div>
                                                  <div class="form-group col-md-6">
                                                    
                                                  <label for="nombre">Unidad de medida</label>
                                                     <select name="und_med" class="form-control">
                                                      <?php foreach ($data["unidad_medida"] as $key => $value) {?>
                                                        <option value="<?php echo ($value["id"])?>">
                                                            <?php echo $value["descripcion"]; ?></option>
                                                      <?php

                                                         } ?>
                                                    </select>
                                                  </div>
                                                  <div class="form-group col-md-12">
                                                    
                                                  <label for="nombre">Descripción</label>
                                                    <textarea class="form-control" rows="3" placeholder="Ingrese detalle ..." name="desc"></textarea>
                                                  </div>
                                                  </div>
                                                </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                                                    <button type="button" onclick="guardar()" class="btn btn-outline-primary">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   


                     <div class="modal fade text-left" id="eliminartipoproducto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
                                     aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                                <div class="modal-header bg-danger white">
                                                    <h4 class="modal-title white" id="eliminartipoproducto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Eliminar tipo de producto</font></font></h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerca">
                                                        <span aria-hidden="true"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">×</font></font></span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">¿Está seguro que quiere eliminar?</font></font></h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Si</font></font></button>
                                                    <button type="button" class="btn btn-outline-danger"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">No</font></font></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


<script type="text/javascript">
    function registrarnuevo(){
        $("#producto").modal("show");
        $("#myModalLabel1").text('Agregar nuevo producto')
    }

    function editar(idtipo){
        $("#myModalLabel1").text('Editar tipo de producto')
        $("#tipoproducto").modal();
        $.ajax({
            url : base_url + 'Tipo_producto/traer_info',
            type : 'POST',
            dataType : 'json',
            data : {id : idtipo},
            success : function(data){
                console.log()
                $("#id").val(data["tipo"][0]["id"]);
                $("#nombre").val(data["tipo"][0]["descripcion"]);
                $("#tipoproducto").modal("hide");
            },error : function(e){

            }
        });
    }
 
    function eliminar(idtipo){
        // $("#myModalLabel10").text('Eliminar tipo de producto')
        // $("#eliminartipoproducto").modal();
        
            var r = confirm("Desea Eliminar!");
            if (r == true) {
               $.ajax({
                url : base_url + 'Tipo_producto/eliminar',
                type : 'POST',
                dataType : 'json',
                data : {id : idtipo},
                success : function(data){ 
                    
                },error : function(e){

                }
            });
          } else {
               
          }
    }

    function guardar(){
        $.ajax({
            url : base_url + 'Productos/guardar',
            type : 'POST',
            dataType : 'json',
            data : $("#formulario").serialize(),
            success : function(data){
                $("#formulario").trigger("reset")
                $("#tipoproducto").modal("hide");
            },error : function(e){

            }

        })
    }
</script>