
        </div>
      </div>
    </div>




<div id="modal_eliminar" class="modal"  tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" style="font-weight: bolder;">¿ESTAS SEGURO QUE DESEA ELIMINAR?</h5>

			</div>
			<div class="modal-body">
				<input type="hidden" value="" id="id_eliminar" name="id_eliminar">
				<p style="text-align: center;">Una vez eliminado ya no se podra recuperar</p>
			</div>
			<div class="modal-footer">
				<button type="button" id="modal_boton_eliminar" onclick="eliminar_datos()" class="btn btn-danger">Si, Acepto</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_cargar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">


<div class="modal-dialog " role="document">



  <div style="
position: relative;

top: 40%;
">
  <div class="loader-wrapper">
							<div class="loader-container">
								<div class="ball-pulse loader-danger">
									<div></div>
									<div></div>
									<div></div>
								</div>
							</div>
		
						</div>
					</div>


</div>
</div>



<div id="cargar_detalle_producto" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detalle de Pedido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style=" ">
              <div class="row">
              	 <div class="col-md-12">
              	 	<div class="table-responsive">
						<table class="table" >
						  <thead >
						    <tr style="border: 1px solid black">
						      <th class="col_" style="width: 30px;">N°</th>
						      <th class="col_"  style="width:200px !important;">Desc. de producto</th>
						      <th class="col_"  style="width: 100px;" >Cantidad</th>
						      <th class="col_"  style="width: 80px;">Precio</th>
						      <th class="col_"  style="width: 80px;">Subtotal</th>

						  

						    </tr>
						  </thead>
              <tbody id="cuerpo_detalle_pedido">
                
              </tbody>

						</table>
              	 	</div>
              	 </div>

              </div>

              <div style="margin-top: 20px;" class="row">
              	<div class="col-md-7">
              		
              	</div>
              	<div class="col-md-1">
              		<p style="margin-top: 10px;font-weight: bold;">Total</p>
              	</div>
              	<div class="col-md-1">
              		<label style="margin-top: 10px;margin-left: 40px;">S/</label>
              	</div>
              	<div class="col-md-2">
              		<input type="text" style="height: 10px !important;margin-top:10px;" readonly="true" name="monto_total" class="form-control" id="monto_total">
              	</div>
              </div>
          	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>
      </div>
    </div>
  </div>
</div>

    
  </body>
<script src="<?php echo base_url() ?>public/jspublic.js"></script>

<script type="text/javascript">
	 function cargar_detalle(id)
  {
  // alert(id);
        let total=0;
        var html="";
        let nombre="";
        $("#cargar_detalle_producto").modal();    

        $.post(base_url+"Registro_pedido/cargar_detalle_pedido",{"id_pedido":id},function(response){
                 console.log(response);
                 for (var i = 0; i < response.length; i++) {
                  

                             html+="<tr>";
                         html+="<td>"+(i+1)+"</td>";
                      
                         if(response[i]["producto_tipo_estado"].toString()=="1"){
                        nombre=response[i]["producto_descripcion"]+" "+response[i]["tipo_conexion_descripcion"];
                         }else{

                       nombre=response[i]["producto_descripcion"];

                         }
                         html+="<td>"+nombre+"</td>";
                         html+="<td>"+response[i]["detalle_pedido_cantidad"]+"</td>";
                         html+="<td>"+response[i]["detalle_pedido_precio"]+"</td>";
                         html+="<td>"+response[i]["detalle_pedido_subtotal"]+"</td>";
                         html+="</tr>";

                         total+=parseFloat(response[i]["detalle_pedido_subtotal"]);
                 }

                 $("#cuerpo_detalle_pedido").empty().append(html);
                 $("#monto_total").val(total.toFixed(2));

        },"json");

  }

</script>

<?php include('Layout/js.php'); ?>  
</html>


    