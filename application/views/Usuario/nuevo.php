 <!-----------------------------footer-------------------------------- -->
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title"></h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      <div class="card-text">





<div class="grid simple ">
<div class="grid-title no-border">

<!-----------------------------footer-------------------------------- -->
<div class="grid simple ">
<div class="grid-title no-border">

<form onsubmit="return guardar_info()" id="formulario" name="formulario" >
	<input type="hidden" id="id" name="id" value="">
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>
				Nombre Completo
			</label>
			<input type="text" required="true" autocomplete="off" name="usu_nombre" id="usu_nombre" class="form-control">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>
				Apellido Completo
			</label>
			<input type="text" required="true" autocomplete="off" name="usu_apellido" id="usu_apellido" class="form-control">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>
				Usuario
			</label>
			<input type="text" required="true" autocomplete="off" name="usuario" id="usuario" class="form-control">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>
				Clave
			</label>
			<input type="text" required="true" autocomplete="off" name="clave" id="clave" class="form-control">
		</div>
	</div>
		<div class="col-md-4">
		<div class="form-group">
			<label>
				Perfil
			</label>
			<select required="true" class="form-control" id="perfil" name="perfil">
				<option value="">SELECCIONAR</option>
				<?php 
                         foreach ($data["perfiles"] as $key => $value) {
                         	echo "<option value='".$value["per_id"]."'>".$value["per_descripcion"]."</option>";
                         }

				?>
			</select>
		</div>
	</div>

		<div class="col-md-4">
		<div class="form-group">
			<label>
				Fecha de Nacimiento
			</label>
			<input type="date" required="true" autocomplete="off" name="usu_fecha_nacimiento" id="usu_fecha_nacimiento" class="form-control">
		</div>
	</div>

		<div class="col-md-4">
		<div class="form-group">
			<label>
				Documento de identidad
			</label>
			<input maxlength="8" type="text" required="true" autocomplete="off" name="usu_documento_identidad" id="usu_documento_identidad" class="form-control">
		</div>
	</div>

		<div class="col-md-4">
			<div class="form-group">
				<label>
					Celular
				</label>
				<input maxlength="9" type="text" required="true" autocomplete="off" name="usu_celular" id="usu_celular" class="form-control">
			</div>
		</div>
			<div class="col-md-4">
		<div class="form-group">
			<label>
				Celular Alternativo
			</label>
			<input maxlength="9" type="text" required="true" autocomplete="off" name="usu_celular_alternativo" id="usu_celular_alternativo" class="form-control">
		</div>
	</div>
		<div class="col-md-4">
		<div class="form-group">
			<label>
				Correo Electronico
			</label>
			<input  type="text" required="true" autocomplete="off" name="usu_correo" id="usu_correo" class="form-control">
		</div>
	</div>


<div class="col-md-4">
		<div class="form-group">
			<label>
				Capacidad
			</label>
			<input  type="number" min="1" required="true" autocomplete="off" name="capacidad" id="capacidad" class="form-control">
		</div>
	</div>













</div>

  <br>
			  <center><a href="<?php echo  base_url();?>Usuario"><button class="btn btn-danger" type="button" >Cancelar</button><a> <button class="btn btn-primary" id="boton_agregar_modulo" onclick="guardar_info()">Guardar</button></center>

</form>

<!-----------------------------footer-------------------------------- -->
</div>
</div>


      </div>
    </div>
  </div>
</section>  


<!-----------------------------footer-------------------------------- -->





<script type="text/javascript">
<?php 
if(isset($data["id"])){ ?>

$(function(){

	$.post(base_url+"Usuario/mostrar",{"id":"<?php echo $data["id"] ;?>"},function(data){
            $("#usuario").attr("readonly",true)
		$("#id").val(data[0]["usu_id"]);
	$("#usu_nombre").val(data[0]["usu_nombre"]);
	$("#usu_apellido").val(data[0]["usu_apellido"]);
	$("#usu_fecha_nacimiento").val(data[0]["usu_fecha_nacimiento"]);
	$("#usu_correo").val(data[0]["usu_correo"]);
	$("#usu_celular_alternativo").val(data[0]["usu_celular_alternativo"]);
	$("#usu_celular").val(data[0]["usu_celular"]);	
	$("#usu_documento_identidad").val(data[0]["usu_documento_identidad"]);	
	

$("#capacidad").val(data[0]["usu_cantidad_max"]);

		$("#usuario").val(data[0]["usu_usuario"]);
			$("#clave").val(data[0]["usu_clave"]);
				$("#perfil").val(data[0]["usu_perfil"]);
	},"json");
});

<?php } ?>
	function guardar_info() {

  $("#button_guardar").text("Guardando...");
                $("#button_guardar").attr("disabled",true);
		 $.post(base_url+"Usuario/guardar",$("#formulario").serialize(),function(data){

		 	  if(data["estado"]){
               window.location=base_url+"Usuario";
		 	  }else{

		 	  	toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"](data["Mensaje"]);


                $("#button_guardar").text("GUARDAR");
                $("#button_guardar").attr("disabled",false);

		 	  }

		 },"json");

		return false;
	}
</script>