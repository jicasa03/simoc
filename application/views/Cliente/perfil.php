
<style type="text/css">
  
  *,
*:before,
*:after {
  box-sizing: border-box;
  outline: none;
}

body {
  background: #1565C0;
  font: 14px/1 "Open Sans", helvetica, sans-serif;
  -webkit-font-smoothing: antialiased;
}

.box {
  height: 120px;
  width: 120px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  background: white;
  border-radius: 100%;
  overflow: hidden;
  border: 2px solid #1565C0;
}
.box .percent {
  position: absolute;
  left: 0;
  top: 0;
  z-index: 3;
  width: 100%;
  height: 100%;
  display: -webkit-box;
  display: flex;
  display: -webkit-flex;
  -webkit-box-align: center;
          align-items: center;
  -webkit-box-pack: center;
          justify-content: center;
  color: #fff;
  font-size: 64px;
}
.box .water {
  position: absolute;
  left: 0;
  top: 0;
  z-index: 2;
  width: 100%;
  height: 100%;
  -webkit-transform: translate(0, 100%);
          transform: translate(0, 100%);
  background: #1565C0;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
}
.box .water_wave {
  width: 200%;
  position: absolute;
  bottom: 100%;
}
.box .water_wave_back {
  right: 0;
  fill: #c7eeff;
  -webkit-animation: wave-back 1.4s infinite linear;
          animation: wave-back 1.4s infinite linear;
}
.box .water_wave_front {
  left: 0;
  fill: #1565C0;
  margin-bottom: -1px;
  -webkit-animation: wave-front 0.7s infinite linear;
          animation: wave-front 0.7s infinite linear;
}

@-webkit-keyframes wave-front {
  100% {
    -webkit-transform: translate(-50%, 0);
            transform: translate(-50%, 0);
  }
}

@keyframes wave-front {
  100% {
    -webkit-transform: translate(-50%, 0);
            transform: translate(-50%, 0);
  }
}
@-webkit-keyframes wave-back {
  100% {
    -webkit-transform: translate(50%, 0);
            transform: translate(50%, 0);
  }
}
@keyframes wave-back {
  100% {
    -webkit-transform: translate(50%, 0);
            transform: translate(50%, 0);
  }
}


</style>







<div class="row">
	<div class="col-md-6 col-6">
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title" style="font-weight: bold;">PERFIL CLIENTE</h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      <div class="row">
          <div class="col-md-8">
            <div style="margin-top: 20px;"></div>
             <h6 class="text-center" style="font-weight: bold;"><label id="nombre"></label></h6>
             <h6  class="text-center" style="font-weight: bold;"><label id="apellido"></label></h6>
             <h6  class="text-center" style=""><label id="telefono"></label></h6>

          </div>
          <div class="col-md-4">
            <img src="https://alumni.crg.eu/sites/default/files/default_images/default-picture_0_0.png" class="rounded-circle" alt="Cinque Terre" style="width:90px; height:90px;"> 
          </div>
      </div>
      <div class="row" style="margin-top: 10px;">
        <div class="col-md-12">
          <h6  class="text-center" style="">San Martín/San Martin/Tarapoto</h6>
          <h6  class="text-center" style="">partido alto/jr. caceres #158</h6>
           <div class="form-group">
             <label>Referencia</label>
               <textarea class="form-control" id="referencia" name="referencia"></textarea>
           </div>
        </div>
      </div>

    </div>
</div>
</section>
</div>
	<div class="col-md-6 col-6">
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title" style="font-weight: bold;">SEGUIMIENTO</h4>
  </div>
  <div class="card-content">
    <div class="card-body">
<div class="row">
<div style="margin-top: 50px;margin-bottom: 70px" class="col-md-4">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" style="display: none;">
  <symbol id="wave">
    <path d="M420,20c21.5-0.4,38.8-2.5,51.1-4.5c13.4-2.2,26.5-5.2,27.3-5.4C514,6.5,518,4.7,528.5,2.7c7.1-1.3,17.9-2.8,31.5-2.7c0,0,0,0,0,0v20H420z"></path>
    <path d="M420,20c-21.5-0.4-38.8-2.5-51.1-4.5c-13.4-2.2-26.5-5.2-27.3-5.4C326,6.5,322,4.7,311.5,2.7C304.3,1.4,293.6-0.1,280,0c0,0,0,0,0,0v20H420z"></path>
    <path d="M140,20c21.5-0.4,38.8-2.5,51.1-4.5c13.4-2.2,26.5-5.2,27.3-5.4C234,6.5,238,4.7,248.5,2.7c7.1-1.3,17.9-2.8,31.5-2.7c0,0,0,0,0,0v20H140z"></path>
    <path d="M140,20c-21.5-0.4-38.8-2.5-51.1-4.5c-13.4-2.2-26.5-5.2-27.3-5.4C46,6.5,42,4.7,31.5,2.7C24.3,1.4,13.6-0.1,0,0c0,0,0,0,0,0l0,20H140z"></path>
  </symbol>
</svg>
<div class="box">
  <div class="percent">
    <div class="percentNum" id="count" style="color: black;font-size: 25px;"> 0</div>
    <div class="percentB" style="color: black;font-size: 25px;">%</div>
  </div>
  <div id="water" class="water">
    <svg viewBox="0 0 560 20" class="water_wave water_wave_back">
      <use xlink:href="#wave"></use>
    </svg>
    <svg viewBox="0 0 560 20" class="water_wave water_wave_front">
      <use xlink:href="#wave"></use>
    </svg>
  </div>
</div>
</div>
<div class="col-md-8">
      <h5 class="text-center" style="font-weight: bold;">Pedido Aproximado</h5>
        <h6 class="text-center" style="">27 de enero de 2020</h6>
</div>
</div>
<div class="row d-flex justify-content-center" style="margin-top: 18px;">
    <div class="col-md-5">
      Promedio de consumo :
    </div>
    <div class="col-md-7">
      30 dias
    </div>


      <div class="col-md-5">
       Tipo de balon :
    </div>
    <div class="col-md-7">
      Premiun
    </div>

      <div class="col-md-5">
    peso : 
    </div>
    <div class="col-md-7">
     10 KG
    </div>

      <div class="col-md-5">
     Numero de familia :
    </div>
    <div class="col-md-7">
     6 
    </div>
  </div>
  <br><br>
*El seguimiento es solo si compra gas


    </div>
</div>
</section>



</div>
</div>
<div class="row">
		<div class="col-md-12 col-12">
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title"></h4>
  </div>
  <div class="card-content">
    <div class="card-body">

       <div class="row">
          <div class="col-md-12">
             <table class="table">
              <thead>
                <tr>
                  <th scope="col">Cod. pedido</th>
                  <th scope="col">Fecha</th>
          
                  <th scope="col">Direccion</th>
                  <th scope="col">Repartidor</th>
                  <th scope="col">Estado</th>
                  <th scope="col">Acción</th>


                </tr>
              </thead>
              <tbody id="cuerpo_pedido">
               
              </tbody>
            </table>
          </div>
       </div>
    </div>
</div>
</section>
</div>
</div>



<script type="text/javascript">
var idcleinte="<?php echo $data["id"]; ?>";


  var cnt=document.getElementById("count"); 
var water=document.getElementById("water");
var percent=cnt.innerText;
var interval;
interval=setInterval(function(){ 
  percent++; 
  cnt.innerHTML = percent; 
  water.style.transform='translate(0'+','+(100-percent)+'%)';
  if(percent==100){
    clearInterval(interval);
  }
},250);
</script>
<script type="text/javascript" src="<?php echo base_url();?>public/modulo/cliente.js"></script>
