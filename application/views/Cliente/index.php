 <!-----------------------------footer-------------------------------- -->
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title"></h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      <div class="card-text">





<div class="grid simple ">
<div class="grid-title no-border">

<!-----------------------------footer-------------------------------- -->


	<a href="<?php echo  base_url();?>Cliente/nuevo"><button class="btn btn-primary">NUEVO CLIENTE</button></a>

<br>
<br>
<div class="row">
	<div class="col-md-12">
		<table class="table" id="example3">
			<thead>
			<tr>
			<th>#</th>
			<th>Nombre</th>
			<th>Telefono</th>
			<th>Correo</th>
            <th>Tipo de Cliente</th>
			<th>Acción</th>
			</tr>
			</thead>
			<tbody>

			<?php
			 foreach ($data["lista"] as $key => $value) {
			
			 	echo '<tr class="odd gradeX">';
			echo '<td>'.($key+1).'</td>';
			echo '<td>'.$value["cliente_nombres"]." ".$value['cliente_apellido'].'</td>';
			echo '<td>'.$value["cliente_telefono"].'</td>';
            echo '<td>'.$value["cliente_correo"].'</td>';
            echo '<td>'.$value["tipo_cliente_descripcion"].'</td>';

			echo '<td class="center">
            <a href="'.base_url().'Cliente/mostrar_perfil/'.$value["cliente_id"].'"><button title="Editar"  class="btn btn-icon btn-primary">   <i class="la la-user" style="color: white"></i> </button> </a>
<a href="'.base_url().'Cliente/editar/'.$value["cliente_id"].'"><button title="Editar"  class="btn btn-icon btn-success"><i class="
la la-pencil" style="color:white"></i> </button> </a>
<a title="Eliminar" class="btn btn-icon btn-danger" onclick="eliminar('.$value["cliente_id"].')">
<i class="la la-trash" style="color:white"></i></a></td>';
			echo '</tr>';
			 }



			?>



			</tbody>
		</table>
	</div>
</div>


<!-----------------------------footer-------------------------------- -->
</div>
</div>


      </div>
    </div>
  </div>
</section>  


<!-----------------------------footer-------------------------------- -->


<script type="text/javascript">


function eliminar(id){
 
    $("#id_eliminar").val(id);
  $("#modal_eliminar").modal({
  keyboard: false,
      backdrop:'static',

	
});


}

function eliminar_datos(){
    $("#modal_boton_eliminar").text("Eliminando...");
    $("#modal_boton_eliminar").attr("disable",true);
    $.post(base_url+"Cliente/delete_modulo",{"id":$("#id_eliminar").val()},function (data) {
		console.log(data);
		if(data["estado"]){
            location.reload();
		}else{
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["error"]("ERROR AL REGISTRAR");


            $("#boton_agregar_modulo").text("AGREGAR MODULO");
            $("#boton_agregar_modulo").attr("disabled",false);
		}
    },"json");

}

	
$(document).ready(function() {
	$('#test2').on( "click",function() {

        $('#form_modulo')[0].reset();
		$("#quick-access").css("bottom","0px");
    });
	 var oTable3 = $('#example3').dataTable( {
	   "sDom": "<'row'<'col-md-6'l <'toolbar'>><'col-md-6'f>r>t<'row'<'col-md-12'p i>>",
        			"oTableTools": {

		},
         "dom": '<"pull-left"f><"pull-right"l>tip',
      
				"oLanguage":{
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
},
    });
	    });
</script>
