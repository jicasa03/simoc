 <!-----------------------------footer-------------------------------- -->
 <section id="description" class="card">
  <div class="card-header">
    <h4 class="card-title"></h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      <div class="card-text">





<div class="grid simple ">
<div class="grid-title no-border">

<!-----------------------------footer-------------------------------- -->
		<form method="POST" id="form_modulo" onsubmit="return guardar_info()" >
			<input type="hidden"  name="cliente_id" id="cliente_id">
			<div class="row">
		    <div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">Nombres Completos</label>
					<input type="text" class="form-control" required="true" name="cliente_nombres" id="cliente_nombres" autofocus="true" value="">
				  </div>
				</div>
			    <div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">Apellidos Completos</label>
					<input type="text" class="form-control" required="true" name="cliente_apellido" id="cliente_apellido" autofocus="true" value="">
				  </div>
				</div>
				 <div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">Telefono</label>
					<input type="text" class="form-control" required="true" name="cliente_telefono" id="cliente_telefono" autofocus="true" value="">
				  </div>
				</div>
					 <div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">Telefono Alternativo</label>
					<input type="text" class="form-control" name="cliente_telefono_alternativo" id="cliente_telefono_alternativo" autofocus="true" value="">
				  </div>
				</div>
					 <div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">Correo</label>
					<input type="text" class="form-control"  name="cliente_correo" id="cliente_correo" autofocus="true" value="">
				  </div>
				</div>

					 <div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">Documento de Identidad</label>
					<input type="text" class="form-control" name="cliente_documento_identidad" id="cliente_documento_identidad" autofocus="true" value="">
				  </div>
				</div>

						<div class="col-md-4">
					<div class="form-group">
						<label class="control-label mb-10 text-left">Tipo de cliente</label>

						 <select class="form-control" name="tipo_cliente_id" id="tipo_cliente_id">
						 	<?php
					

                               foreach ($data["select_cliente"] as $key => $value) {
                                echo "<option value='".$value["tipo_cliente_id"]."'>".$value["tipo_cliente_descripcion"]."</option>";
                               }
						 	?>
						 </select>
					</div>
				</div>


					 <div class="col-md-4">
				<div class="form-group">

					<label class="control-label mb-10 text-left">Fecha de Nacimiento</label>
					<input type="date" class="form-control" name="cliente_fecha_nacimiento" id="cliente_fecha_nacimiento" autofocus="true" value="">
				  </div>
				</div>
			</div>
	
			  <br>
			  <center><a href="<?php echo  base_url();?>Cliente"><button class="btn btn-danger" type="button" >Cancelar</button><a> <button class="btn btn-primary" id="boton_agregar_modulo" onclick="guardar_info()">Guardar</button></center>
		   </form>


<!-----------------------------footer-------------------------------- -->
</div>
</div>


      </div>
    </div>
  </div>
</section>  


<!-----------------------------footer-------------------------------- -->



<script type="text/javascript">
	<?php

     if(isset($data["id"]))
     {?>


     $(function(){

     	$.post(base_url+"Cliente/update_modulo",{"cliente_id":"<?php echo $data['id']?>"},function(data){
              console.log(data);
              $("#cliente_id").val(data[0]["cliente_id"]);
              $("#cliente_nombres").val(data[0]["cliente_nombres"]);

              $("#cliente_apellido").val(data[0]["cliente_apellido"]);
              $("#cliente_telefono").val(data[0]["cliente_telefono"]);
              $("#tipo_cliente_id").val(data[0]["tipo_cliente_id"]);
              $("#cliente_documento_identidad").val(data[0]["cliente_documento_identidad"]);
              $("#cliente_fecha_nacimiento").val(data[0]["cliente_fecha_nacimiento"]);
              $("#cliente_correo").val(data[0]["cliente_correo"]);
              $("#cliente_telefono_alternativo").val(data[0]["cliente_telefono_alternativo"]);

              $("#cliente_fecha_nacimiento").attr('readonly',true);

             


     	},"json");
     });




     <?php } ?>


     function guardar_info(){

     $("#boton_agregar_modulo").text("Guardando...");
$("#boton_agregar_modulo").attr("disabled",true);
     $.post(base_url+"Cliente/guardar_modulo",$("#form_modulo").serialize(),function(data){
           if(data["estado"]){
                  location.href =base_url+"Cliente";
           }
           else{
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-bottom-right",
			  "preventDuplicates": false,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
			toastr["error"]("ERROR AL REGISTRAR");


                  $("#boton_agregar_modulo").text("AGREGAR MODULO");
					$("#boton_agregar_modulo").attr("disabled",false);

           }
     },"json");
       return false;

	}
</script>
