<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Mapeo_cliente extends BaseController {
	public function __construct() {
		parent::__construct();
	}

	public function index(){
	   	$data=array();
		$data["titulo_descripcion"]="Mapeo de Clientes";
	
		$this->vista("Mapeo_cliente/index",$data);
	}
}