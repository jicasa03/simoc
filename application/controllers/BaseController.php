<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//require_once("convertirAletra.php");
class BaseController extends CI_Controller {



public function __construct() {
  parent::__construct();
    $this->load->model("Ciudades_m");
    $this->load->model("Mantenimiento_m");

  date_default_timezone_set("America/Lima");
  session_start();
  if(!isset($_COOKIE["usuario"])|| $_COOKIE["usuario"]==""){
     header("Location: ".base_url()."Login");
     exit();
  }

}

public function vista($cuerpo,$data=array()){
  if(!isset($_COOKIE["usuario"])|| $_COOKIE["usuario"]==""){
    header('Location: '.base_url());
    exit();
  }

  // print_r($_COOKIE);exit();
  $id=$_COOKIE["perfil"];

  $lista = $this->db->get_where('modulo', array('mod_estado' => 1,'mod_padre' => 0))->result_array();
  foreach ($lista as $key => $value) {
    $sql=     "select modulo.* from modulo inner join permisos on(modulo.mod_id=permisos.per_modulo) inner join perfil on(perfil.per_id=permisos.per_perfil) where permisos.per_perfil=". $id." and modulo.mod_padre=".$value["mod_id"]." and modulo.mod_estado=1 order by mod_orden asc";
    $query = $this->db->query(
$sql
    )->result_array();
    $lista[$key]["modulos"] = $query;
  }


  $data["titulo"]='';
 // $data["logo_empresa"]='';
  //$data["titulo_corto"]= '';   
  $this->load->view('Layout',compact('lista','data'));
  $this->load->view($cuerpo);
  $this->load->view('Footer');

}

public function provincia($id){
  $provincia = $this->Ciudades_m->getProvincia($id); 
  $html="";
  $html.= "<option value=''>Selecionar Provincia</option>";
  foreach ($provincia as $value) {
    $html.= "<option value='".$value["id_provincia"]."'>".$value["descripcion"]."</option>";
  }
  echo $html;
}

public function distrito($id){
  $distrito = $this->Ciudades_m->getDistrito($id); 
  $html="";
  $html.= "<option value=''>Selecionar Distrito</option>";
  foreach ($distrito as $value) {
    $html.= "<option value='".$value["id_distrito"]."'>".$value["descripcion"]."</option>";
  }
  echo $html;
}


public function departamento()
{
    $distrito = $this->db->query("select * from departamento where estado=1")->result_array();
  $html="";
  $html.= "<option value=''>Selecionar</option>";
  foreach ($distrito as $value) {
    $html.= "<option value='".$value["id_departamento"]."'>".$value["descripcion"]."</option>";
  }
  echo  $html;
}


public function tipo_producto()
{

$tipo=$this->db->query("select * from  tipo_producto where tipo_producto_estado=1")->result_array();
  $html="";
  $html.= "<option value=''>Selecionar</option>";
  foreach ($tipo as $value) {
    $html.= "<option value='".$value["tipo_producto_id"]."'>".$value["tipo_producto_descripcion"]."</option>";
  }
  echo  $html;
}



public function tipo_conexion()
{

$tipo=$this->db->query("select * from  tipo_conexion where tipo_conexion_estado=1")->result_array();
  $html="";
  $html.= "<option value=''>Selecionar</option>";
  foreach ($tipo as $value) {
    $html.= "<option value='".$value["tipo_conexion_id"]."'>".$value["tipo_conexion_descripcion"]."</option>";
  }
  echo  $html;
}
function redim($ruta1,$ruta2,$ancho,$alto){
  $datos=getimagesize ($ruta1);
  $ancho_orig = $datos[0]; # Anchura de la imagen original
  $alto_orig = $datos[1];    # Altura de la imagen original
  $tipo = $datos[2];

    if ($tipo==1){ # GIF
      if (function_exists("imagecreatefromgif"))
        $img = imagecreatefromgif($ruta1);
      else
        return false;
    }
    else if ($tipo==2){ # JPG
      if (function_exists("imagecreatefromjpeg"))
        $img = imagecreatefromjpeg($ruta1);
      else
        return false;
    }
    else if ($tipo==3){ # PNG
      if (function_exists("imagecreatefrompng"))
        $img = imagecreatefrompng($ruta1);
      else
        return false;
    }
    # Se calculan las nuevas dimensiones de la imagen
    if ($ancho_orig>$alto_orig){
      $ancho_dest=$ancho;
      $alto_dest=($ancho_dest/$ancho_orig)*$alto_orig;
    }else{
      $alto_dest=$alto;
      $ancho_dest=($alto_dest/$alto_orig)*$ancho_orig;
    }
    $img2=@imagecreatetruecolor($ancho_dest,$alto_dest) or $img2=imagecreate($ancho_dest,$alto_dest);
    @imagecopyresampled($img2,$img,0,0,0,0,$ancho_dest,$alto_dest,$ancho_orig,$alto_orig) or imagecopyresized($img2,$img,0,0,0,0,$ancho_dest,$alto_dest,$ancho_orig,$alto_orig);
    // Crear fichero nuevo, según extensión.
    if ($tipo==1) // GIF
    if (function_exists("imagegif"))
      imagegif($img2, $ruta2);
    else
      return false;
    if ($tipo==2) // JPG
    if (function_exists("imagejpeg"))
      imagejpeg($img2, $ruta2);
    else
      return false;
    if ($tipo==3)  // PNG
    if (function_exists("imagepng"))
      imagepng($img2, $ruta2);
    else
      return false;
    return true;
  }
  

  public function antecedente_personal(){
    $traer_antecedentes["item"] = $this->Mantenimiento_m->consultar_antecedentes();
    $traer_antecedentes["total_count"] = 1;
    echo json_encode($traer_antecedentes);
  }
  //define( 'API_ACCESS_KEY', 'AAAAtg4DsDw:APA91bHtzuF73GqDb8fs3-4tV54m9XzrfzslTcR8flrdLRCIptXUWeCqLhD1oBbyRgaLGqKfWM8ze2pI9xHAkr7VwaWUHtXOlHGiL6EhmcEd6-SFlSMPppUeZHuQtSo5ktkX9Gej_sYi' );
public function enviar_notificacion_cliente($titulo,$mensaje,$cliente){



   $fcmMsg = array(
            'body' =>$mensaje,
            'title' =>$titulo,
            // "image"=> $image,
             "sound"=> "default",
          );

      $cliente=$this->db->query("select cliente_token_firebase from cliente where cliente_id=".$cliente)->row_array();

              $singleID= $cliente["cliente_token_firebase"];
          $fcmFields = array(
            //'registration_ids' => $cliente,
            'to'=>$singleID,
                  'priority' => 'high',

           
            'notification' => $fcmMsg,

          /*  'apns'=>array(
                       'payload'=>array(
                               'aps'=>array(
                                    'mutable-content'=>'1'
                               )
                         ),

                       'fcm_options'=>array(
                             'image'=> "https://www.grupoesconsultores.com//consultoria/public/perfil/blob.1516897315.jpg",
                        )
                  )*/
          );


          $api_key='AAAAe7bs4ls:APA91bH3o1BjlgJjzuWK-fS6v16hxkRphmWAgehkJmG3-S_NUXnCzPoPDtXYhR1s3wl1n2ObNx3o1CYGYe_jju5ZNKU3Fvmrw5MtEG5FsCcxRviiSap3VbCl0VHHPH30jOPQhJsJsu-f';
          $headers = array(
            'Authorization: key=' .$api_key,
            'Content-Type: application/json'
          );
           
        //   echo json_encode($fcmFields);

          $ch = curl_init();
          curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
          curl_setopt( $ch,CURLOPT_POST, true );
          curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
          curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
          curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
          curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
          $result = curl_exec($ch );
        curl_close( $ch );
         // echo $result . "\n\n";


}

public function enviar_notificacion_deliverista($titulo,$mensaje,$usuario){



   $fcmMsg = array(
            'body' =>$mensaje,
            'title' =>$titulo,
            // "image"=> $image,
             "sound"=> "default",
          );

      $cliente=$this->db->query("select usu_token_firebase from usuario where usu_id=".$usuario)->row_array();

              $singleID= $cliente["usu_token_firebase"];
          $fcmFields = array(
            //'registration_ids' => $cliente,
            'to'=>$singleID,
                  'priority' => 'high',

           
            'notification' => $fcmMsg,

          /*  'apns'=>array(
                       'payload'=>array(
                               'aps'=>array(
                                    'mutable-content'=>'1'
                               )
                         ),

                       'fcm_options'=>array(
                             'image'=> "https://www.grupoesconsultores.com//consultoria/public/perfil/blob.1516897315.jpg",
                        )
                  )*/
          );


          $api_key='AAAAe7bs4ls:APA91bH3o1BjlgJjzuWK-fS6v16hxkRphmWAgehkJmG3-S_NUXnCzPoPDtXYhR1s3wl1n2ObNx3o1CYGYe_jju5ZNKU3Fvmrw5MtEG5FsCcxRviiSap3VbCl0VHHPH30jOPQhJsJsu-f';
          $headers = array(
            'Authorization: key=' .$api_key,
            'Content-Type: application/json'
          );
           
        //   echo json_encode($fcmFields);

          $ch = curl_init();
          curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
          curl_setopt( $ch,CURLOPT_POST, true );
          curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
          curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
          curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
          curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
          $result = curl_exec($ch );
        curl_close( $ch );
         // echo $result . "\n\n";


}



public function cargar_notificaciones($titulo,$descripcion,$perfil,$url)
{
  $sql="SELECT * from usuario where

usuario.usu_perfil=".$perfil." and usuario.usu_estado=1";

$lista=$this->Mantenimiento_m->consulta3($sql);



foreach ($lista as $key => $value) {
    $data1=array(
            "descripcion"=>$descripcion,
            "fecha"=>date('Y-m-d H:i:s'),
            "url"=>$url,
            "id_usuario"=>$value["usu_id"],
            "id"=>"1",
            "nombre"=> $titulo

        );

   // print_r($data1);
        $this->db->insert('notificacion', $data1); 
      //  print_r($data1);

}

}


}
