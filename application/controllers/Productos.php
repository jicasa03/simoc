<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Productos extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function index(){
        $data=array();
        $data["titulo_descripcion"]="Productos";
        $data["producto"]=$this->db->query("Select p.id, tp.descripcion as tpdesc, p.descripcion as pdesc from producto as p, tipo_producto as tp where p.estado=1 and p.id_tipo_producto=tp.id order by p.id")->result_array();
        $data["tipo_producto"]=$this->db->query("select id, descripcion from tipo_producto where estado=1 order by id")->result_array();  
        $data["unidad_medida"]=$this->db->query("select id, descripcion, abreviatura from unidad_medida where estado=1 order by id")->result_array();  
        $data["categoria"]=$this->db->query("select id, descripcion from categoria where estado=1 order by id")->result_array();  
        $this->vista("Producto/Producto",$data);
    }

    public function guardar(){
        
        $data=array(
            "id_tipo_producto"=>$_POST["tipo_prod"],
            "descripcion"=>$_POST["producto"],
            "id_categoria"=>$_POST["cat"],
            "idunidad"=>$_POST["und_med"],
    );

        if ($_POST["id"] == "") { 
            $this->db->insert("producto",$data);  
        }else{ 
            $this->db->where('id', $_POST["id"]);
            $this->db->update("producto",$data);
        }
        
        echo json_encode(1);
    }

    public function nuevo(){
        $data=array();
        $data["titulo_descripcion"]="Tipo_Producto";
        //$data["tipo"]=$this->db->query("select * from tipo_producto where estado=1")->result_array();
        $this->vista("Tipo_Producto/nuevo",$data);

    }

    public function traer_info(){
        $id=$_POST["id"]; 
        $data["tipo"]=$this->db->query("select * from tipo_producto where id=".$id)->result_array();
        die(json_encode($data));
        //$this->vista("Tipo_Producto/editar",$data);
    }
 

    public function eliminar(){
      $data=array("estado"=>0);

            $this->db->where('id', $_POST["id"]);
            $this->db->update("tipo_producto",$data);
        
        echo json_encode(1);   

    }    

}