<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";

class Registro_producto extends BaseController {

 public function __construct() {
        parent::__construct();
       	
      
    }


    public function index()
    {
       $data["titulo_descripcion"]="Lista de Productos";
       	$data["lista"] = $this->db->query("SELECT *
FROM
producto
INNER JOIN tipo_producto ON producto.tipo_producto_id = tipo_producto.tipo_producto_id
INNER JOIN unidad_medida ON producto.unidad_medida_id = unidad_medida.unidad_medida_id
where producto_estado=1")->result_array();

    	$this->vista("Registro_producto/index",$data);
    }
	public function nuevo()
	{

     $data=array();
      $data["titulo_descripcion"]="Nuevo Producto";
       $data["select_tipo_producto"]=$this->db->query("select * from tipo_producto where tipo_producto_estado=1")->result_array();
       $data["select_unidad_medida"]=$this->db->query("select * from unidad_medida where unidad_medida_estado=1")->result_array();
       $data["select_tipo_conexion"]=$this->db->query("select * from tipo_conexion where tipo_conexion_estado=1")->result_array();


      //$data["tabla"]=$this->Mantenimiento_m->consulta3("select * from perfiles where estado=1");
	  $this->vista("Registro_producto/nuevo",$data);
	}
    public function guardar_modulo(){
		if ($this->input->is_ajax_request()){

			$response=array();
			$producto_tipo_estado="";
			
			if(isset($_POST["producto_tipo_estado"])){
                 $producto_tipo_estado=1;
			}else{
                  $producto_tipo_estado=0;

			}

			$ultimoId = "";
			if($_POST["producto_id"]==""){
				$data = array(
				'producto_descripcion' => $_POST["producto_descripcion"],
				'producto_tipo_estado' => $producto_tipo_estado,
				'unidad_medida_id' => $_POST["unidad_medida_id"],
				'tipo_producto_id' => $_POST["tipo_producto_id"]
				);
				$estado=$this->db->insert('producto', $data);
				$response["estado"]=true;
				$response["mensaje"]="Se Ingresó Correctamente";
					$ultimoId =$this->db->insert_id();
			}else{
			$data = array(
				'producto_descripcion' => $_POST["producto_descripcion"],
				'producto_tipo_estado' =>$producto_tipo_estado,
				'unidad_medida_id' => $_POST["unidad_medida_id"],
				'tipo_producto_id' => $_POST["tipo_producto_id"]
				);
				$this->db->where('producto_id',$_POST["producto_id"]);
				$estado=$this->db->update('producto', $data);
				$response["estado"]=true;
				$response["mensaje"]="Se Actualizó Correctamente ";
				$ultimoId =$_POST["producto_id"];
			}
              

				$data = array(
								'precio_estado' => 0
								);
							$this->db->where('producto_id',$ultimoId);
				$estado=$this->db->update('precio', $data);


               if($producto_tipo_estado==1)
               {
                  foreach ($_POST["tabla_tipo_conexion_id"] as $key => $value) {
                  	$data = array(
							'precio_monto' => $_POST["tabla_precio"][$key],
							'producto_id' =>$ultimoId ,
							'tipo_conexion_id'=>$value
							);
							$estado=$this->db->insert('precio', $data);
                  }
                      

               }else{

						$data = array(
							'precio_monto' => $_POST["precio_producto"],
							'producto_id' =>$ultimoId 
							);
							$estado=$this->db->insert('precio', $data);

               }







			echo json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}

	function update_modulo(){
		$query = $this->db->get_where('producto', array('producto_id' => $_POST["id"]))->result_array();
        $sql="select * from precio where precio_estado=1 and producto_id=".$_POST["id"];
		$data=$this->db->query($sql)->result_array();
		$query["detalle"]=$data;
		echo json_encode($query);exit();
	}

	function delete_modulo(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'producto_estado' => 0
				);
			$this->db->where('producto_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('producto', $data);




				$data = array(
								'precio_estado' => 0
								);
							$this->db->where('producto_id', $_POST["id"]);
				$estado=$this->db->update('precio', $data);


		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}

		public function editar($id)
	{
		 $data=array();
      $data["titulo_descripcion"]="Actualizar Producto";
      //$data["tabla"]=$this->Mantenimiento_m->consulta3("select * from perfiles where estado=1");
      $data["id"]=$id;
  $data["select_tipo_producto"]=$this->db->query("select * from tipo_producto where tipo_producto_estado=1")->result_array();
       $data["select_unidad_medida"]=$this->db->query("select * from unidad_medida where unidad_medida_estado=1")->result_array();
       $data["select_tipo_conexion"]=$this->db->query("select * from tipo_conexion where tipo_conexion_estado=1")->result_array();

  	  $this->vista("Registro_producto/nuevo",$data);


	}




}
