<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//require_once "BaseController.php";
class Login extends CI_Controller {
  public function __construct() {
    parent::__construct();
    date_default_timezone_set("America/Lima");
    $this->load->model("Mantenimiento_m");
    session_start();
  }

  public function cerrar_session(){
    session_destroy();
    $this->borrarCookie();
    header('Location: '.base_url());
  }
  public function borrarCookie(){ 
    setcookie ('usuario', '', time()-604800,'/');
    setcookie ('id', '', time()-604800,'/');
    setcookie ('perfil', '', time()-604800,'/');
    setcookie ('name', '', time()-604800,'/');
    setcookie ('login', '', time()-604800,'/');
    setcookie ('imagen', '', time()-604800,'/');

    
    unset ($_COOKIE ["usuario"]);
    unset ($_COOKIE ["id"]);
    unset ($_COOKIE ["perfil"]);
    unset ($_COOKIE ["name"]);
    unset ($_COOKIE ["login"]);
    unset ($_COOKIE ["imagen"]);

  
  }

  public function index(){
    if(!isset($_COOKIE["usuario"])|| $_COOKIE["usuario"]==""){
      $this->load->view('Login/index');
    }else{
      header('Location: '.base_url().'Control');
    }
  }

  public function Iniciar(){ 
    $user = $_POST['usuario'];
    $clave = $_POST['clave'];
    $user=trim($user);
    $user=htmlspecialchars($user);
    $user=stripcslashes($user);
    $user=filter_var($user, FILTER_SANITIZE_STRING);
    $clave=trim($clave);
    $clave=htmlspecialchars($clave);
    $clave=stripcslashes($clave);
    $clave=filter_var($clave, FILTER_SANITIZE_STRING);
    $fila = $this->db->query("select * from usuario where usu_usuario='".$user."' and usu_clave='".$clave."' and usu_estado=1")->row(); 
      if ($fila) {

        $this->setCookie("usuario",$user);
        $this->setCookie("id",$fila->usu_id);
        $this->setCookie("perfil",$fila->usu_perfil);
     
        $this->setCookie("name",$fila->usu_nombre_completo);
        $this->setCookie("login",TRUE);
        $this->setCookie("imagen",$fila->usu_foto);

        echo (1);
      }else{ 
        echo (2);
      }
}


public function setCookie($nombre,$valor){
  setcookie($nombre,$valor,time()+30*24*60*60,'/');
}
}

