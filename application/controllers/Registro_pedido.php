<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Registro_pedido extends BaseController {
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$data=array();
		$data["titulo_descripcion"]="Registro Pedido";
		 $data["select_cliente"]=$this->db->query("select * from tipo_cliente where tipo_cliente_estado=1")->result_array();
		$this->vista("Registro_pedido/index",$data);
	}

	public function cargar_producto()
	{
        $response=array();

        $id=$_POST["id"];
       $data= $this->db->query("SELECT
*
FROM
producto
INNER JOIN unidad_medida ON producto.unidad_medida_id = unidad_medida.unidad_medida_id
WHERE producto.producto_estado=1 and  tipo_producto_id=".$id)->result_array();

       echo json_encode($data);
                   


	}

    public function cargar_precio()
    {
    	$id=$_POST["id"];
    	$response=$this->db->query("SELECT * FROM precio where producto_id=".$id." and precio_estado=1")->row_array();

    	echo  json_encode($response);exit();

    }
    public function cargar_tipo_conexion()
    {
    	$id=$_POST["id"];
    	$response=$this->db->query("SELECT *
FROM
precio
INNER JOIN tipo_conexion ON precio.tipo_conexion_id = tipo_conexion.tipo_conexion_id
WHERE producto_id=".$id." and precio_estado=1")->result_array();

    	echo  json_encode($response);exit();
    }

    public function guardar_pedido()
    {
        //print_r($_POST);
           $response=[];
           $postdata = file_get_contents("php://input");
                $request = json_decode($postdata,true);
               $cliente=array();
               $direccion=array();
               $detalle_pedido=array();
                 parse_str($request["cliente"], $cliente);
                   parse_str($request["direccion"], $direccion);
                     parse_str($request["detalle_pedido"], $detalle_pedido);
                      
                       $data=array(
                       "cliente_id"=>$cliente["cliente_id"],
                       "direccion_descripcion"=>$direccion["direccion_descripcion"],
                       "direccion_piso"=>$direccion["direccion_piso"],
                       "direccion_interior"=>$direccion["direccion_interior"],
                       "direccion_latitud"=>$direccion["latitude_direccion"],
                       "direccion_longitud"=>$direccion["longitude_direccion"],
                       "direccion_lote"=>$direccion["direccion_lote"],
                       "direccion_referencia"=>$direccion["direccion_referencia"],
                       "distrito_id"=>$direccion["distrito"],
                       "direccion_sector"=>$direccion["direccion_sector"],
                       "direccion_manzana"=>$direccion["direccion_manzana"]
                       );
                     $direccion_id="";
                    if($direccion["direccion_id"]=="")
                    {
                        $this->db->insert("direccion",$data);
                        $direccion_id = $this->db->insert_id();
                    }else{
            
                           $this->db->where("direccion_id",$direccion["direccion_id"]);
                          $this->db->update("direccion",$data);
                          $direccion_id=$direccion["direccion_id"];
                    }



                    $data=array(
                     "pedido_monto_total"=>$detalle_pedido["total_detalle"],
                     "pedido_fecha_inicio"=>date("Y-m-d H:i:s"),
                     "cliente_id"=>$cliente["cliente_id"],
                     "direccion_id"=>$direccion_id ,
                    // "usu_id"=>$direccion["repartidor_id"],
                     "usu_id"=>$direccion["repartidor_id"],

                     "pedido_sector"=>$direccion["direccion_sector"],
                     "pedido_manzana"=>$direccion["direccion_manzana"],
                     "pedido_lote"=>$direccion["direccion_lote"],
                     "pedido_direccion"=>$direccion["direccion_descripcion"],
                     "pedido_piso"=>$direccion["direccion_piso"],
                     "pedido_interior"=>$direccion["direccion_interior"],

                     "pedido_referencia"=>$direccion["direccion_referencia"],

                     "pedido_tipo_web"=>1,

                     "pedido_latitude"=>$direccion["latitude_direccion"],
                     "pedido_longitude"=>$direccion["longitude_direccion"],
                     "pedido_estado_seguimiento"=>1,
                     "distrito_id"=>$direccion["distrito"],
                     "registro_usuario_id"=>$_COOKIE["id"]

               

                    );
                   $this->db->insert("pedido",$data);
                        $pedid_id = $this->db->insert_id();

                     //   print_r($detalle_pedido["cargar_id_precio"] );
                  foreach ($detalle_pedido["cargar_id_precio"] as $key => $value) {
                     $subtotal=(double)$detalle_pedido["cargar_precio"][$key]*(double)$detalle_pedido["cargar_cantidad"][$key];
                        $data=array(
                           "pedido_id"=>$pedid_id,
                           "precio_producto_id"=>$value,
                           "detalle_pedido_precio"=>$detalle_pedido["cargar_precio"][$key],
                           "detalle_pedido_cantidad"=>$detalle_pedido["cargar_cantidad"][$key],
                           "detalle_pedido_detalle"=>$detalle_pedido["cargar_detalle"][$key],
                           "detalle_pedido_subtotal"=> $subtotal,


                        );    
                        $this->db->insert("detalle_pedido",$data);
                        $detalle_id = $this->db->insert_id();
                    

                  }
                  


                  $response["estado"]=1;
                  $response["mensaje"]="Se Registro Correctamente";
                  echo json_encode($response);
                  exit();

                     //print_r($cliente);
                     //print_r($detalle_pedido);
                     //print_r($direccion);

          
    }


    public function cargar_pedido_deliverista()
    {            

             $response=array();
             $response=$this->db->query("SELECT * from usuario where usuario.usu_estado=1 and usuario.usu_perfil=2")->result_array();
               foreach ($response as $key => $value) {
                   $sql="SELECT
pedido.pedido_id,
HOUR(pedido.pedido_fecha_inicio) as 'hora',
MINUTE(pedido.pedido_fecha_inicio) as 'minuto',
pedido.pedido_monto_total,
pedido.pedido_fecha_inicio,
pedido.cliente_id,
pedido.pedido_estado,
pedido.pedido_estado_pedido,
pedido.direccion_id,
pedido.usu_id,
pedido.pedido_sector,
pedido.pedido_manzana,
pedido.pedido_lote,
pedido.pedido_direccion,
pedido.pedido_piso,
pedido.pedido_interior,
pedido.pedido_referencia,
pedido.pedido_tipo_web,
pedido.pedido_latitude,
pedido.pedido_longitude,
pedido.pedido_estado_seguimiento,
pedido.distrito_id,
distrito.id_distrito,
distrito.descripcion,
distrito.observacion,
distrito.id_provincia,
distrito.estado,
cliente.cliente_id,
cliente.cliente_nombres,
cliente.cliente_apellido,
cliente.cliente_telefono,
cliente.tipo_cliente_id,
cliente.cliente_documento_identidad,
cliente.cliente_latitud,
cliente.cliente_longitud,


cliente.cliente_fecha_registro,
cliente.cliente_telefono_alternativo




FROM
pedido
INNER JOIN cliente ON pedido.cliente_id = cliente.cliente_id
INNER JOIN distrito ON pedido.distrito_id = distrito.id_distrito
WHERE
                            pedido.usu_id=".$value["usu_id"]." and pedido.pedido_estado_seguimiento=1
                          and  pedido.pedido_estado=1

                            order by pedido.pedido_fecha_inicio desc";
                    $carga=$this->db->query($sql)->result_array();
                    $response[$key]["direccion"] =  $carga;
               }


               echo json_encode($response);exit();

    }



public function cargar_pedido()
{

  $sql="SELECT
*
FROM
pedido
INNER JOIN cliente ON pedido.cliente_id = cliente.cliente_id
INNER JOIN distrito ON pedido.distrito_id = distrito.id_distrito
INNER JOIN usuario ON pedido.usu_id = usuario.usu_id
WHERE
pedido.pedido_estado = 1 and date(pedido.pedido_fecha_inicio)='".date("Y-m-d")."'
ORDER BY
pedido.pedido_fecha_inicio DESC ";


            $data=$this->db->query($sql)->result_array();

            echo json_encode($data);exit();
}


public function cargar_detalle_pedido()
{

     $pedido_id=$_POST["id_pedido"];
              
         $sql="SELECT
detalle_pedido.detalle_pedido_id,
producto.producto_descripcion,

tipo_conexion.tipo_conexion_descripcion,
detalle_pedido.detalle_pedido_cantidad,
detalle_pedido_precio,
tipo_producto.tipo_producto_descripcion,
detalle_pedido.detalle_pedido_subtotal,
producto.producto_tipo_estado
FROM
detalle_pedido
INNER JOIN precio ON detalle_pedido.precio_producto_id = precio.precio_producto_id
INNER JOIN producto ON precio.producto_id = producto.producto_id
INNER JOIN tipo_producto ON producto.tipo_producto_id = tipo_producto.tipo_producto_id

LEFT JOIN tipo_conexion ON precio.tipo_conexion_id=tipo_conexion.tipo_conexion_id

WHERE
detalle_pedido.pedido_id =".$pedido_id;    
       $datos=$this->db->query($sql)->result_array();
       echo json_encode($datos);exit();

}


public function cargarsector()
{
  $sector=$this->db->query("SELECT 
direccion.direccion_sector as 'sector'
from direccion
")->result_array();

  $response=array();
  foreach ($sector as $key => $value) {
    $response[$key]=$value["sector"];
   
  }

  echo json_encode($response);



}








}

?>