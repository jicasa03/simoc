<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Control extends BaseController {
	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$data=array();
		$data["titulo_descripcion"]="Panel de control";
		$data["pedido_llamada"]=$this->db->query("select count(pedido_id) as 'contar' from pedido where pedido_estado=1 and pedido_tipo_web=1 and date(pedido.pedido_fecha_inicio)='".date("Y-m-d")."'")->result_array();
			$data["pedido_app"]=$this->db->query("select count(pedido_id) as 'contar' from pedido where pedido_estado=1 and pedido_tipo_web=0 and date(pedido.pedido_fecha_inicio)='".date("Y-m-d")."'")->result_array();
	$data["eliminado"]=$this->db->query("select count(pedido_id) as 'contar' from pedido where pedido_estado=0 and date(pedido.pedido_fecha_inicio)='".date("Y-m-d")."'")->result_array();

		$data["pendiente"]=$this->db->query("select count(pedido_id) as 'contar' from pedido where pedido_estado=1 and pedido_estado_seguimiento=0 and date(pedido.pedido_fecha_inicio)='".date("Y-m-d")."'")->result_array();
		$this->vista("Control/index",$data);
	}
	 public function notificaciones1()
    {  

 
        $nombre="";
        $descripcion="";
        $fecha="";
        $imagen="";
        $url="";
        $sql="select * from notificacion where estado=2 and id_usuario=".$_COOKIE["id"]." LIMIT 1";
        $html="";
        $id="";
        $arp=1;
         $cantidad=0;
         $id_notificacion="";
         $id1="";
        $data=$this->Mantenimiento_m->consulta($sql);
        //print_r($data);
       
        foreach ($data as $key => $value)
         {
            $url=$value->url;
            $nombre=$value->nombre;
            $fecha=$value->fecha;
            $descripcion=$value->descripcion;
            $imagen=$value->imagen;
            $id=$value->id_notificacion;
            $id1=$value->id;
            $arp=2;
        
        }

 /*  <div class="notification-messages success">
<div class="user-profile">
<img src="assets/img/profiles/h.jpg" alt="" data-src="assets/img/profiles/h.jpg" data-src-retina="assets/img/profiles/h2x.jpg" width="35" height="35">
</div>
<div class="message-wrapper">
<div class="heading">
You haveve got 150 messages
</div>
<div class="description">
150 newly unread messages in your inbox
</div>
<div class="date pull-left">
An hour ago
</div>
</div>
<div class="clearfix"></div>
</div>*/

        $sql="select  * from notificacion where estado<>0 and id_usuario=".$_COOKIE["id"];
     
        $data1=$this->Mantenimiento_m->consulta($sql);
         $tal= count($data1); 
        // print_r($tal);
          $html.='';
       foreach ($data1 as $key => $value)
        { $fecha = date_create($value->fecha);
              
          $a="'".$value->id_notificacion."','".$value->url."','".$value->id."'";
           $html.='<a   onclick="notificacion('.$a.')" title="'.$value->nombre.'-'.$value->descripcion.'">
                          <div class="media">
                                          <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                                          <div class="media-body">';
 

                $html.='<h6 class="media-heading">
                              '.$value->nombre.'
                                </h6>
                                <p class="notification-text font-small-3 text-muted">
                              '.$value->descripcion.'
                              </p><small>
                                              <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">
                               '.date_format($fecha, "F j, Y, g:i a").'
                                </time></small>
                                          </div>
                                        </div></a>';
              /*$html.='<div class="media-body" onclick="notificacion('.$a.')">
                  <a> <strong>'.$value->nombre.'</strong>-'.$value->descripcion.'
                  </a><div class="media-annotation">'.date_format($fecha, "F j, Y, g:i a").'</div>*/
                            
                       


       }

      

           $html.='
                <script>
                   function notificacion(id_notificacion,url,id)
                       {
                        $.post(base_url+"Control/borrar_notificacion",{id:id_notificacion},function(data){
    
                       });

                       window.location.href = base_url+url+"/"+id;
                      /* $.post(base_url+url,{id:id},function(data){
                   
                            $("#cont_sistema").empty().html(data);
                             });
                       $(".dropdown").removeClass("open");*/

                     }
                   </script>
                ';

         $datos=array("notificaciones"=>$html,"cantidad"=>$tal,"nombre"=>$nombre,"fecha"=>$fecha,"imagen"=>$imagen,"descripcion"=>$descripcion
        ,"url"=>$url,"arp"=>$arp,"id"=>$id1,"id_notificacion"=>$id);
         echo json_encode($datos);
         if($id!=""){
          $this->Mantenimiento_m->consulta1("update notificacion set estado=1 where id_notificacion=".$id);
         }
        
    }
        public function borrar_notificacion()
    { // echo $_POST['id'];exit();
        $this->Mantenimiento_m->consulta1("update notificacion set estado=0 where id_notificacion=".$_POST['id']);
    }

}