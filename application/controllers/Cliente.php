<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";

class Cliente extends BaseController {

 public function __construct() {
        parent::__construct();
       	
      
    }


    public function index()
    {
       $data["titulo_descripcion"]="Lista de Clientes";
       	$data["lista"] = $this->db->query("SELECT
*
FROM
cliente
JOIN tipo_cliente
ON cliente.tipo_cliente_id = tipo_cliente.tipo_cliente_id
where cliente_estado=1")->result_array();

    	$this->vista("Cliente/index",$data);
    }
	public function nuevo()
	{

     $data=array();
      $data["titulo_descripcion"]="Nuevo Cliente";
       $data["select_cliente"]=$this->db->query("select * from tipo_cliente where tipo_cliente_estado=1")->result_array();
      //$data["tabla"]=$this->Mantenimiento_m->consulta3("select * from perfiles where estado=1");
	  $this->vista("Cliente/nuevo",$data);
	}
    public function guardar_modulo(){
		if ($this->input->is_ajax_request()){

			$response=array();
			
			if($_POST["cliente_id"]==""){
				$data = array(
				'cliente_nombres' => $_POST["cliente_nombres"],
				'cliente_apellido' => $_POST["cliente_apellido"],
				'cliente_telefono' => $_POST["cliente_telefono"],
				'tipo_cliente_id' => $_POST["tipo_cliente_id"],
				'cliente_documento_identidad' => $_POST["cliente_documento_identidad"],
				'cliente_fecha_nacimiento' => $_POST["cliente_fecha_nacimiento"],
				'cliente_fecha_registro' => date('Y-m-d H:i:s'),
				'cliente_telefono_alternativo' => $_POST["cliente_telefono_alternativo"],
				'cliente_correo' => $_POST["cliente_correo"]


				);
				$estado=$this->db->insert('cliente', $data);
				$response["estado"]=true;
				$response["mensaje"]="Se Ingresó Correctamente ";
			}else{
				$data = array(
				'cliente_nombres' => $_POST["cliente_nombres"],
				'cliente_apellido' => $_POST["cliente_apellido"],
				'cliente_telefono' => $_POST["cliente_telefono"],
				'tipo_cliente_id' => $_POST["tipo_cliente_id"],
				'cliente_documento_identidad' => $_POST["cliente_documento_identidad"],

		
				'cliente_telefono_alternativo' => $_POST["cliente_telefono_alternativo"],
				'cliente_correo' => $_POST["cliente_correo"]


				);
				$this->db->where('cliente_id',$_POST["cliente_id"]);
				$estado=$this->db->update('cliente', $data);
				$response["estado"]=true;
				$response["mensaje"]="Se Actualizó Correctamente ";
			}
			echo json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}

	function update_modulo(){
		$query = $this->db->get_where('cliente', array('cliente_id' => $_POST["cliente_id"]))->result();
		echo json_encode($query);
	}

	function delete_modulo(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'cliente_estado' => 0
				);
			$this->db->where('cliente_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('cliente', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}

		public function editar($id)
	{
		 $data=array();
      $data["titulo_descripcion"]="Actualizar Cliente";
      //$data["tabla"]=$this->Mantenimiento_m->consulta3("select * from perfiles where estado=1");
      $data["id"]=$id;
       $data["select_cliente"]=$this->db->query("select * from tipo_cliente where tipo_cliente_estado=1")->result_array();
  	  $this->vista("Cliente/nuevo",$data);


	}


	public function buscar_cliente()
	{
           $search=$_GET["term"];

            $sql="SELECT CONCAT(cliente.cliente_nombres,' ',cliente.cliente_apellido) as 'value',cliente.cliente_id as 'id',
             CONCAT(cliente.cliente_telefono,'/',cliente.cliente_telefono_alternativo) as 'telefono'
from cliente
where cliente.cliente_estado=1
and (CONCAT(cliente.cliente_nombres,' ',cliente.cliente_apellido) like '%".$search."%'
or cliente.cliente_telefono like '%".$search."%'

or cliente.cliente_documento_identidad like '%".$search."%'
or cliente.cliente_telefono_alternativo like '%".$search."%'

or cliente.cliente_correo like '%".$search."%'
)";

              $cargar=$this->db->query($sql)->result_array();
                echo json_encode($cargar);exit();





	}


	public function buscar_direccion_cliente()
	{

		$sql="SELECT direccion.*,
		    distrito.descripcion as 'distrito_descripcion',
		    provincia.descripcion as 'provincia_descripcion',
		    provincia.id_provincia as 'id_provincia',
		    departamento.descripcion as 'departamento_descripcion',
		    departamento.id_departamento as 'id_departamento'

				FROM
				direccion
				INNER JOIN distrito ON direccion.distrito_id = distrito.id_distrito
				INNER JOIN provincia ON distrito.id_provincia = provincia.id_provincia
				INNER JOIN departamento ON provincia.id_departamento = departamento.id_departamento
				where 
				direccion.cliente_id=".$_POST["id"];
          


        $data=$this->db->query($sql)->result_array();

   echo json_encode($data);exit();
  

	}

	public function mostrar_perfil($id)
	{
 //print_r(base64_decode('c8h44e2n79laef60aivao04ko'));
           $data["id"]=$id;
	
		  $data["titulo_descripcion"]="Perfil del cliente";
		  $this->vista("Cliente/perfil",$data);
	}
	public function cargarperfil()
	{
		$id=$_POST["idcliente"];
		$cliente=$this->db->query("select * from cliente where cliente_id=".$id)->row_array();
        $direccion=$this->db->query('SELECT  CASE WHEN usuario.usu_nombre IS null then "" ELSE usuario.usu_nombre END as "repartidor",
pedido.pedido_id as "cod_pedido",
DATE_FORMAT(pedido.pedido_fecha_inicio, "%d/%m/%Y") as "fecha_pedido",
CASE 
  WHEN pedido.pedido_estado_pedido=1 THEN "Asignado"
	WHEN pedido.pedido_estado_pedido=2 THEN "Recogido"
	WHEN pedido.pedido_estado_pedido=3 THEN "Entregado"
	ELSE "Eliminado" END as "estado",
CONCAT(pedido.pedido_direccion,"|piso:",pedido.pedido_piso,"|sector:",pedido.pedido_sector )as "direccion"
FROM
pedido
LEFT JOIN usuario ON 
pedido.usu_id = usuario.usu_id
where pedido.cliente_id='.$id)->result_array();




		$response=array();
		$response["cliente"]=$cliente;
		$response["direccion"]=$direccion;
		echo  json_encode($response);
	}


}
