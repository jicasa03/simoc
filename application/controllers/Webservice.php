<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT');
class Webservice extends CI_Controller {
	public function __construct() {
		parent::__construct();
      $this->load->model("Mantenimiento_m");
	}

public function enviar_notificacion_cliente($titulo,$mensaje,$cliente){



   $fcmMsg = array(
            'body' =>$mensaje,
            'title' =>$titulo,
            // "image"=> $image,
             "sound"=> "default",
          );

      $cliente=$this->db->query("select cliente_token_firebase from cliente where cliente_id=".$cliente)->row_array();

              $singleID= $cliente["cliente_token_firebase"];
          $fcmFields = array(
            //'registration_ids' => $cliente,
            'to'=>$singleID,
                  'priority' => 'high',

           
            'notification' => $fcmMsg,

          /*  'apns'=>array(
                       'payload'=>array(
                               'aps'=>array(
                                    'mutable-content'=>'1'
                               )
                         ),

                       'fcm_options'=>array(
                             'image'=> "https://www.grupoesconsultores.com//consultoria/public/perfil/blob.1516897315.jpg",
                        )
                  )*/
          );


          $api_key='AAAAe7bs4ls:APA91bH3o1BjlgJjzuWK-fS6v16hxkRphmWAgehkJmG3-S_NUXnCzPoPDtXYhR1s3wl1n2ObNx3o1CYGYe_jju5ZNKU3Fvmrw5MtEG5FsCcxRviiSap3VbCl0VHHPH30jOPQhJsJsu-f';
          $headers = array(
            'Authorization: key=' .$api_key,
            'Content-Type: application/json'
          );
           
        //   echo json_encode($fcmFields);

          $ch = curl_init();
          curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
          curl_setopt( $ch,CURLOPT_POST, true );
          curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
          curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
          curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
          curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
          $result = curl_exec($ch );
        curl_close( $ch );
         // echo $result . "\n\n";


}

public function enviar_notificacion_deliverista($titulo,$mensaje,$usuario){



   $fcmMsg = array(
            'body' =>$mensaje,
            'title' =>$titulo,
            // "image"=> $image,
             "sound"=> "default",
          );

      $cliente=$this->db->query("select usu_token_firebase from usuario where usu_id=".$usuario)->row_array();

              $singleID= $cliente["usu_token_firebase"];
          $fcmFields = array(
            //'registration_ids' => $cliente,
            'to'=>$singleID,
                  'priority' => 'high',

           
            'notification' => $fcmMsg,

          /*  'apns'=>array(
                       'payload'=>array(
                               'aps'=>array(
                                    'mutable-content'=>'1'
                               )
                         ),

                       'fcm_options'=>array(
                             'image'=> "https://www.grupoesconsultores.com//consultoria/public/perfil/blob.1516897315.jpg",
                        )
                  )*/
          );


          $api_key='AAAAe7bs4ls:APA91bH3o1BjlgJjzuWK-fS6v16hxkRphmWAgehkJmG3-S_NUXnCzPoPDtXYhR1s3wl1n2ObNx3o1CYGYe_jju5ZNKU3Fvmrw5MtEG5FsCcxRviiSap3VbCl0VHHPH30jOPQhJsJsu-f';
          $headers = array(
            'Authorization: key=' .$api_key,
            'Content-Type: application/json'
          );
           
        //   echo json_encode($fcmFields);

          $ch = curl_init();
          curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
          curl_setopt( $ch,CURLOPT_POST, true );
          curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
          curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
          curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
          curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
          $result = curl_exec($ch );
        curl_close( $ch );
         // echo $result . "\n\n";


}



public function cargar_notificaciones($titulo,$descripcion,$perfil,$url)
{
  $sql="SELECT * from usuario where

usuario.usu_perfil=".$perfil." and usuario.usu_estado=1";

$lista=$this->Mantenimiento_m->consulta3($sql);



foreach ($lista as $key => $value) {
    $data1=array(
            "descripcion"=>$descripcion,
            "fecha"=>date('Y-m-d H:i:s'),
            "url"=>$url,
            "id_usuario"=>$value["usu_id"],
            "id"=>"",
            "nombre"=> $titulo

        );

   // print_r($data1);
        $this->db->insert('notificacion', $data1); 
      //  print_r($data1);

}

}
	

	public function registrar_cliente()
	{
     $response=[];
		   $postdata = file_get_contents("php://input");
                $request = json_decode($postdata,true);
             $verificacion=$this->db->query('select * from cliente where cliente_correo="'.$request['informacionclave']['correo'].'"')->result_array();
             if(count($verificacion)>0){
                         $response['estado']=0;
                     $response['mensaje']='ERROR CORREO YA EXISTE';
                             echo json_encode($response);
                             exit();

             }
             $data=array(
              'cliente_nombres'=>$request['informacionpersonal']['nombres'],
              'cliente_apellido'=>$request['informacionpersonal']['apellidos'],
              'cliente_telefono'=>$request['informacionpersonal']['celular'],
              'tipo_cliente_id'=>1,
              'cliente_fecha_nacimiento'=>$request['informacionpersonal']['fechanacimiento'],
              'cliente_fecha_registro'=>date('Y-m-d H:i:s'),
              'cliente_telefono_alternativo'=>$request['informacionpersonal']['celular2'],
              'cliente_correo'=>$request['informacionclave']['correo'],
              'cliente_usuario'=>$request['informacionclave']['usuario'],
              'cliente_clave'=>$request['informacionclave']['clave']
        
             );
          $estado=$this->db->insert('cliente',$data);

             if($estado==1)
             {

               $response['estado']=1;
               $response['mensaje']='Se registro correctamente';
             }else{
               $response['estado']=0;
               $response['mensaje']='Error al registrar usuario';
             }
           
                echo json_encode($response);
     exit();
	}


  public function loguear()
  {
             $response=[];
             $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);
             $verificacion=$this->db->query('select cliente_nombres,cliente_apellido,cliente_telefono,cliente_correo,cliente_usuario,cliente_telefono_alternativo,cliente_id from cliente where cliente_correo="'.$request['usuario'].'" and cliente_clave="'.$request['clave'].'"  and cliente_estado=1')->row_array();
             if(count( (array)$verificacion)>0){
              $this->db->query("update cliente set cliente_token_firebase='".$request['token']."' where cliente_id=".$verificacion["cliente_id"]);
              $response['estado']=1;
               $response['mensaje']='SE INICIO SESION CORRECTMENTE';
               $response['datos']=$verificacion;
             }else{
                $response['estado']=0;
               $response['mensaje']='ERROR AL INICIAR SESION';

             }

              echo json_encode($response);
     exit();

  }


  public function registrar_ubicacion()
  {


         $response=[];
         $postdata = file_get_contents("php://input");
         $request = json_decode($postdata,true);

         $data=array(
             'direccion_descripcion'=>$request['datos']['direccion'],
             'direccion_piso'=>$request['datos']['piso'],
             'direccion_interior'=>$request['datos']['interior'],
             'direccion_latitud'=>$request['latitud'],
             'direccion_longitud'=>$request['longitud'],
             'direccion_lote'=>$request['datos']['lote'],
             'direccion_foto'=>$request['imagen'],
             'direccion_referencia'=>$request['datos']['referencia'],
             'direccion_sector'=>$request['datos']['sector'],
             'direccion_manzana'=>$request['datos']['manzana'],
             "cliente_id"=>$request['idcliente'],
             "distrito_id"=>$request['datos']['distrito_id']


         );

        $estado= $this->db->insert('direccion',$data);
     
             if($estado==1)
             {

               $response['estado']=1;
               $response['mensaje']='Se registro correctamente';
             }else{
               $response['estado']=0;
               $response['mensaje']='Error al registrar usuario';
             }
           
                echo json_encode($response);
     exit();




  }

  public function mostrar_ubicacion()
  {
          $response=[];
             $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);
       $datos=$this->db->query('SELECT direccion_id,direccion_descripcion,direccion_piso,direccion_interior,direccion_latitud,direccion_longitud,direccion_lote,direccion_foto,direccion_referencia,direccion_sector,direccion_manzana FROM direccion where direccion_estado=1 and direccion.cliente_id='.$request['id_cliente'])->result_array();

       echo json_encode($datos);exit();
  }

  public function eliminardireccion()
  {
       $response=[];
             $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);

             $sql='update direccion set direccion_estado=0  where  direccion_id='.(int)$request['id'];
          //   echo $sql;
      $estado=$this->db->query($sql);
     // print_r($estado);
      if($estado==1)
    {
      $response['estado']=1;
      $response['mensaje']="Se elimino correctamente";

    }else{
       $response['estado']=0;
      $response['mensaje']="Error al eliminar";
    }

       echo json_encode($response);exit();
  }

  public function mostrar_departamento()
  {
   $datos=$this->db->query('SELECT * from departamento where estado=1')->result_array();

       echo json_encode($datos);exit();
      

  }
  public function mostrar_provincia()
  {

          $response=[];
             $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);
       $datos=$this->db->query('SELECT * from  provincia where estado=1 and provincia.id_departamento='.$request['id_departamento'])->result_array();

       echo json_encode($datos);exit();





  }
  public function mostrar_distrito()
  {

          $response=[];
             $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);
      $datos=$this->db->query('SELECT * from  distrito where estado=1 and distrito.id_provincia='.$request['id_provincia'])->result_array();

       echo json_encode($datos);exit();





  }
  public function tipo_producto()
  {

        $tipo=$this->db->query("select * from  tipo_producto where tipo_producto_estado=1")->result_array();
        echo json_encode($tipo);exit();
  }
  public function cargar_producto(){

    $response=array();
            $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);
        $id=$request["id"];
                 $data= $this->db->query("SELECT
                                           *
                                            FROM
                                        producto
                                         INNER JOIN unidad_medida ON producto.unidad_medida_id = unidad_medida.unidad_medida_id
                                       WHERE producto.producto_estado=1 and  tipo_producto_id=".$id)->result_array();

       echo json_encode($data);exit();

  }
    public function cargar_tipo_conexion()
    {
        $response=array();
                $postdata = file_get_contents("php://input");
                 $request = json_decode($postdata,true);
            $id=$request["id"];
          $response=$this->db->query("SELECT *
        FROM
        precio
        INNER JOIN tipo_conexion ON precio.tipo_conexion_id = tipo_conexion.tipo_conexion_id
        WHERE producto_id=".$id." and precio_estado=1")->result_array();

      echo  json_encode($response);exit();
    }

     public function cargar_precio()
      {
                $response=array();
                $postdata = file_get_contents("php://input");
                 $request = json_decode($postdata,true);
            $id=$request["id"];
      $response=$this->db->query("SELECT * FROM precio where producto_id=".$id." and precio_estado=1")->row_array();

      echo  json_encode($response);exit();

    }
    public function generar_pedido()
    {
        $response=array();
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata,true);
    //  print_r($request);
        $direccion=$this->db->query("select * from direccion where direccion_id=".$request["id_direccion"])->row_array();
      //  print_r($direccion);
  $total=0;

  foreach ($request["pedido"] as $key => $value) {
      $total+=(double)$value["precio"]*(int)$value["cantidad"];
  }
//echo $total;

           $data=array(
                     "pedido_monto_total"=>$total,
                     "pedido_fecha_inicio"=>date("Y-m-d H:i:s"),
                     "cliente_id"=>$request["id_persona"],
                     "direccion_id"=>$request["id_direccion"] ,
                    // "usu_id"=>$direccion["repartidor_id"],
                  
                     "pedido_sector"=>$direccion["direccion_sector"],
                     "pedido_manzana"=>$direccion["direccion_manzana"],
                     "pedido_lote"=>$direccion["direccion_lote"],
                     "pedido_direccion"=>$direccion["direccion_descripcion"],
                     "pedido_piso"=>$direccion["direccion_piso"],
                     "pedido_interior"=>$direccion["direccion_interior"],

                     "pedido_referencia"=>$direccion["direccion_referencia"],

                     "pedido_tipo_web"=>1,

                     "pedido_latitude"=>$direccion["direccion_latitud"],
                     "pedido_longitude"=>$direccion["direccion_longitud"],
                     "pedido_estado_seguimiento"=>0,
                     "distrito_id"=>$direccion["distrito_id"]

               

                    );
                   $this->db->insert("pedido",$data);
                        $pedid_id = $this->db->insert_id();




                     //   print_r($detalle_pedido["cargar_id_precio"] );
                  foreach($request["pedido"] as $key => $value) {
                     $subtotal=(double)$value["precio"]*(int)$value["cantidad"];
                        $data=array(
                           "pedido_id"=>$pedid_id,
                           "precio_producto_id"=>$value["idprecio"],
                           "detalle_pedido_precio"=>$value["precio"],
                           "detalle_pedido_cantidad"=>$value["cantidad"],
                           "detalle_pedido_detalle"=>"",
                           "detalle_pedido_subtotal"=> $subtotal,


                        );    
                        $this->db->insert("detalle_pedido",$data);
                        $detalle_id = $this->db->insert_id();
                    

                  }
             
             $cliente=$this->db->query("select * from cliente where cliente_id=".$request["id_persona"])->row_array();

                $this->cargar_notificaciones("NUEVO PEDIDO",$cliente["cliente_nombres"]." ".$cliente["cliente_apellido"],1,"Lista_pedido");
                   $response["estado"]=1;
                  $response["mensaje"]="Se Registro Correctamente";

                  echo json_encode($response);
                  exit();
                  

    }

     public function loguear_usuario()
  {
             $response=[];
             $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);
             $verificacion=$this->db->query('select * from usuario where usu_usuario="'.$request['usuario'].'" and usu_clave="'.$request['clave'].'"  and usu_estado=1 and usu_perfil=2')->row_array();
             if(count( (array)$verificacion)>0){
              $this->db->query("update usuario set usu_token_firebase='".$request['token']."' where usu_id=".$verificacion["usu_id"]);
              $response['estado']=1;
               $response['mensaje']='SE INICIO SESION CORRECTMENTE';
               $response['datos']=$verificacion;
             }else{
                $response['estado']=0;
               $response['mensaje']='ERROR AL INICIAR SESION';

             }

              echo json_encode($response);
     exit();

  }

  public function cargar_pedido_deliverista()
  {
   $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);
    $id_conductor=$request["id_usuario"];
                    $sql="SELECT
                    IF(pedido.pedido_fecha_asignacion  IS null,'',TIME(pedido.pedido_fecha_asignacion)) as 'fecha_asignacion',
IF(pedido.pedido_fecha_entrega  IS null,'',TIME(pedido.pedido_fecha_entrega)) as 'fecha_entrega',
                        pedido.pedido_id,
                        pedido.pedido_monto_total,
                        pedido.pedido_fecha_inicio,
                        pedido.cliente_id,
                        pedido.pedido_estado,
                        pedido.pedido_estado_pedido,
                        pedido.pedido_sector,
                        pedido.pedido_manzana,
                        pedido.pedido_lote,
                        pedido.pedido_direccion,
                        pedido.pedido_piso,
                        pedido.pedido_interior,
                        pedido.pedido_referencia,
                        pedido.pedido_tipo_web,
                        pedido.pedido_latitude,
                        pedido.pedido_longitude,
                        pedido.pedido_estado_seguimiento,
                        cliente.cliente_nombres,
                        cliente.cliente_apellido,
                        cliente.cliente_telefono,
                        cliente.tipo_cliente_id,
                        cliente.cliente_documento_identidad,
                        provincia.descripcion AS provincia_descripcion,
                        departamento.descripcion AS departamento_descripcion,
                        distrito.descripcion AS distrito_descripcion,
                        direccion.direccion_foto,
                        cliente.cliente_telefono_alternativo
                        FROM
                        pedido
                        INNER JOIN cliente ON pedido.cliente_id = cliente.cliente_id
                        INNER JOIN distrito ON pedido.distrito_id = distrito.id_distrito
                        INNER JOIN provincia ON distrito.id_provincia = provincia.id_provincia
                        INNER JOIN departamento ON provincia.id_departamento = departamento.id_departamento
                        INNER JOIN direccion ON direccion.cliente_id = cliente.cliente_id AND direccion.distrito_id = distrito.id_distrito AND pedido.direccion_id = direccion.direccion_id
                        WHERE
                        pedido.pedido_estado = 1 AND
                        pedido.pedido_estado_seguimiento = 1
and
                        date(pedido.pedido_fecha_asignacion)='".date("Y-m-d")."'
                        and
                        pedido.usu_id=".$id_conductor."
                        ORDER BY
                        pedido.pedido_fecha_asignacion DESC";




              $data=$this->db->query($sql)->result_array();
                $response=array();
                      foreach ($data as $key => $value) {
                         $response[$key]=$value;

                            $sql1="SELECT
                                    detalle_pedido.detalle_pedido_id,
                                    producto.producto_descripcion,

                                    tipo_conexion.tipo_conexion_descripcion,
                                    detalle_pedido.detalle_pedido_cantidad,
                                    detalle_pedido_precio,
                                    tipo_producto.tipo_producto_descripcion,
                                    detalle_pedido.detalle_pedido_subtotal,
                                    producto.producto_tipo_estado
                                    FROM
                                    detalle_pedido
                                    INNER JOIN precio ON detalle_pedido.precio_producto_id = precio.precio_producto_id
                                    INNER JOIN producto ON precio.producto_id = producto.producto_id
                                    INNER JOIN tipo_producto ON producto.tipo_producto_id = tipo_producto.tipo_producto_id

                                    LEFT JOIN tipo_conexion ON precio.tipo_conexion_id=tipo_conexion.tipo_conexion_id

                                    WHERE
                                    detalle_pedido.pedido_id =".$value["pedido_id"];    

                                   $data1=$this->db->query($sql1)->result_array();

                           $response[$key]["detalle"]=$data1;
                        
                      }


              echo json_encode($response);exit();
  }
  public function pedido_entregado()
  {
  $postdata = file_get_contents("php://input");
             $request = json_decode($postdata,true);

      $pedido_id=$request["id_pedido"];
 //     $deliverista_id=$_POST["deliverista_id"];

      $sql="update pedido set  pedido_fecha_entrega='".date("Y-m-d H:i:s")."' ,pedido_estado_pedido=2 where pedido_id=".$pedido_id;
      $estado=$this->db->query($sql);
       
       $sql="select * from pedido where pedido_id=".$pedido_id;
       $pedido=$this->db->query($sql)->row_array();

       $this->enviar_notificacion_cliente("Universal gas","Pedido entregado gracias por su servicio",$pedido["cliente_id"]);

     //      $this->enviar_notificacion_deliverista("Universal gas","Se te asigno un nuevo pedido",$deliverista_id);

      $response=array();
      $response["estado"]=1;
      $response["mensaje"]="SE REGISTRO CORRECTMENTE";
      echo json_encode($response);exit();





  }

  public function cargar_politicas()
  {
      $resul=$this->db->query("select * from configuracion")->row_array();

          echo json_encode($resul);exit();
  }




}
















