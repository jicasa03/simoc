<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";

class Perfiles extends BaseController {
	public function __construct() {
		parent::__construct();


	}


	public function index()
	{
		$data["titulo_descripcion"]="Lista de Perfiles";
		$data["lista"] = $this->db->query("select * from perfil where per_estado=1")->result_array();
        

          $modulos = $this->db->query("select * from modulo where mod_padre=0 order by mod_orden asc")->result_array();
		foreach ($modulos as $key => $value) {
			$mod = $this->db->query("select * from modulo  where modulo.mod_padre=".$value["mod_id"]." and modulo.mod_estado=1")->result_array();
			$modulos[$key]["lista"] = $mod;
		}
		$data["modulo"]=$modulos;
                

		$this->vista("Perfiles/index",$data);
	}

	public function nuevo()
	{
		$data["titulo_descripcion"]="Nuevo Perfil";
     $this->vista("Perfiles/nuevo",$data);

	}



	public function editar($id)
	{
     
		 $data=array();
      $data["titulo_descripcion"]="Actualizar Perfil";
      //$data["tabla"]=$this->Mantenimiento_m->consulta3("select * from perfiles where estado=1");
      $data["id"]=$id;
       
  	  $this->vista("Perfiles/nuevo",$data);

	}


	function update_modulo(){
		$query = $this->db->get_where('perfil', array('per_id' => $_POST["id"]))->result();
		echo json_encode($query);
	}

	function delete_perfil(){
		if ($this->input->is_ajax_request()){
			$data = array(
				'per_estado' => 0
			);
			$this->db->where('per_id', $_POST["id"]);
			$estado=$this->db->update('perfil', $data);
			$response["estado"]=true;
			echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}
	public function save_perfil(){
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'per_descripcion' => $_POST["perfil"],
				'observacion' => $_POST["descripcion"]
			);
			if($_POST["id"]==""){

				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('perfil', $data);
			}else{
				$this->db->where('per_id',$_POST["id"]);
				$estado=$this->db->update('perfil', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se Actualizó correctamente";
			}

			echo json_encode($response);exit();

		}else{
			$this->load->view('Error/404');
		}
	}

	public function guardar_permiso(){
		if ($this->input->is_ajax_request()){
              $id=$_POST["id_permiso"];

              $response=array();


              $this->db->query("delete from permisos where per_perfil=".$id);
if(isset($_POST["permiso"])){

              foreach ($_POST["permiso"] as $key => $value) {
              	$data=array(
                 "per_perfil"=>$id,
                 "per_modulo"=>$value,
                 "per_nuevo"=>1,
                 "per_modificar"=>1,
                 "per_eliminar"=>1,
                 "per_ver"=>1,
                 "per_imprimir"=>1,

              	);

              	$estado=$this->db->insert('permisos', $data);
              }
}
			 $response["estado"]=true;
			 $response["mensaje"]="Se guardó correctamente";

			 echo json_encode($response);exit();

			}else{
			$this->load->view('Error/404');
		}
	}

	public function mostrar_permisos()
	{
   

     if ($this->input->is_ajax_request()){
     			$id=$_POST["id"];
     			$sql="select * from permisos where per_perfil=".$id;

                 $data=$this->db->query($sql)->result_array();
                 echo json_encode($data);exit();

			}else{
					$this->load->view('Error/404');
				}
		
	}

}
