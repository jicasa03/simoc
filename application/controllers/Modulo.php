<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";

class Modulo extends BaseController {

 public function __construct() {
        parent::__construct();
       	
      
    }


    public function index()
    {
       $data["titulo_descripcion"]="Lista de Módulos";
       	$data["lista"] = $this->db->query("select * from modulo where mod_estado=1")->result_array();
       $data["padres"] = $this->db->query("select * from modulo where mod_padre=0 and mod_estado=1")->result_array();
    	$this->vista("Modulo/index",$data);
    }
	public function nuevo()
	{

     $data=array();
      $data["titulo_descripcion"]="Nuevo Modulo";
       $data["select_modulo_padre"]=$this->db->query("select * from modulo where mod_padre=0 and mod_estado=1")->result_array();
      //$data["tabla"]=$this->Mantenimiento_m->consulta3("select * from perfiles where estado=1");
	  $this->vista("Modulo/nuevo",$data);
	}
    public function guardar_modulo(){
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'mod_descripcion' => $_POST["mod_descripcion"],
				'mod_url' => $_POST["mod_url"],
				'mod_padre' => $_POST["mod_padre"],
				'mod_icono' => $_POST["mod_icono"]
				);
			if($_POST["mod_id"]==""){
				$estado=$this->db->insert('modulo', $data);
				$response["estado"]=true;
				$response["mensaje"]="Se Ingresó Correctamente ";
			}else{
				$this->db->where('mod_id',$_POST["mod_id"]);
				$estado=$this->db->update('modulo', $data);
				$response["estado"]=true;
				$response["mensaje"]="Se Actualizó Correctamente ";
			}
			echo json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}

	function update_modulo(){
		$query = $this->db->get_where('modulo', array('mod_id' => $_POST["id"]))->result();
		echo json_encode($query);
	}

	function delete_modulo(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'mod_estado' => 0
				);
			$this->db->where('mod_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('modulo', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}

		public function editar($id)
	{
		 $data=array();
      $data["titulo_descripcion"]="Actualizar Modulo";
      //$data["tabla"]=$this->Mantenimiento_m->consulta3("select * from perfiles where estado=1");
      $data["id"]=$id;
       $data["select_modulo_padre"]=$this->db->query("select * from modulo where mod_padre=0")->result_array();
  	  $this->vista("Modulo/nuevo",$data);


	}


}
