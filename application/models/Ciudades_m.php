<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ciudades_m extends CI_Model {

	public function getCiudades(){
		$this->db->select("id_departamento,descripcion");
		$this->db->from("departamento"); 
		$this->db->where("estado","1");
		$resultados = $this->db->get();
		return $resultados->result_array();
	}
 	

 	public function getProvincia($id_depa){
		$this->db->select("id_provincia,descripcion");
		$this->db->from("provincia"); 
		$this->db->where("estado","1");
		$this->db->where("id_departamento",$id_depa);
		$resultados = $this->db->get();
		return $resultados->result_array();
	}

	public function getDistrito($id_provincia){
		$this->db->select("id_distrito,descripcion");
		$this->db->from("distrito"); 
		$this->db->where("estado","1");
		$this->db->where("id_provincia",$id_provincia);
		$resultados = $this->db->get();
		return $resultados->result_array();
	}

	public function getTotal($id_distrito){
		$this->db->select("id_distrito,descripcion,id_provincia,id_departamento");
		$this->db->from("distrito"); 
		$this->db->where("estado","1");
		$this->db->where("id_distrito",$id_distrito);
		$resultados = $this->db->get();
		return $resultados->result_array();
	}





 

}