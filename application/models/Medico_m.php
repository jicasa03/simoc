<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medico_m extends CI_Model{
    public function __construct(){
        parent::__construct();
    }


    public function consultar_medico($dni){
    	$this->db->select("*");
		$this->db->from("empleados"); 
		$this->db->where("empleado_dni",$dni);
		$resultados = $this->db->get();
		return $resultados->row();
    }

    public  function getMedicos(){
        $this->db->select("e.empleado_id,e.empleado_nombres,e.empleado_apellidos,e.empleado_dni,e.empleado_direccion,e.empleado_email,e.empleado_telefono,e.empleado_distrito,e.perfil_id,e.empleado_usuario,e.empleado_clave,e.estado as emp_estado,e.empleado_nombre_completo,e.empleado_foto_perfil,e.empleado_fecha_nacimiento,e.empleado_fecha_ingreso,e.empleado_fecha_salida,e.empleado_sueldoplanilla,e.empleado_sexo,e.empresa_sede,e.empleado_universidad,e.nrocolegiatura,e.trabajoanterior,e.lugarpractica,e.facebook,e.instagram,e.empleado_especialidad,p.perfil_id,p.perfil_descripcion,p.estado as per_estado,p.perfil_url,es.id_especialidad,es.descripcion_especialidad,es.estado_especialidad");
        $this->db->from('empleados e');
        $this->db->join('perfiles p', 'e.perfil_id = p.perfil_id');
        $this->db->join('especialidad es', 'e.empleado_especialidad = es.id_especialidad');
        $this->db->where("p.perfil_id",2);
        $aResult = $this->db->get(); 
        return $aResult->result_array();
    }

 
}