<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pacientes_m extends CI_Model{
    public function __construct(){
        parent::__construct();
    }


    public function consultar_paciente($dni){ 
    	$this->db->select("*");
		$this->db->from("paciente"); 
		$this->db->where("paciente_dni",$dni);
		$resultados = $this->db->get();
		return $resultados->row();
    }

    public function ListaPacientes(){
        $this->db->select("*");
        $this->db->from("paciente p");
        $this->db->join('tipopaciente tc','p.paciente_tipopaciente = tc.tipopaciente_id');
        $this->db->join('historia_clinica hc','hc.historiaclinica_idpaciente = p.paciente_id','left');
        $aResult = $this->db->get(); 
        return $aResult->result_array();
    }
 

 
}