<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mantenimiento_m extends CI_Model{
    public function __construct(){
        parent::__construct();
    }


    public function GuardarInformacion($tabla,$datos=array()){
      $r=$this->db->insert($tabla,$datos);
      return $r;
    }

    public function ActualizarInformacion($tabla,$datos=array(),$condicion,$valor){
       $this->db->where($condicion,$valor);
       $r=$this->db->update($tabla,$datos);
       return $r;
    }

    public function consulta($sql){
        $datasesores=$this->db->query($sql);
        return $datasesores->result();
    }
    public function consulta1($sql){
        $this->db->query($sql);
    }

    public function consulta2($sql){
        $datasesores=$this->db->query($sql);
        return $datasesores->row();     
    }
    public function consulta3($sql){
    $datasesores=$this->db->query($sql);
    return $datasesores->result_array();      
    }

    public function consultar_antecedentes(){
        $this->db->select("*");
        $this->db->from("antecedentes"); 
        $this->db->where("antecedentes_estado",1);
        $resultados = $this->db->get();
        return $resultados->result_array();
    }



}

?>