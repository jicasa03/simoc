	function guardar_pedido()
	{

     var json={};
     var contador=0;
      var id_cliente=$("#cliente_id").val();
     if(id_cliente=="")
     {
        
                 alerta_modal("Falta seleccionar un cliente");
        return false;
     }

      $('input[name="cargar_id_precio[]"]').map(function () {
        contador=1;
      }).get();

      if(contador==0)
      {
                     alerta_modal("Se necesita agregar al menos un producto");
        return false;
      }

      if($("#direccion_sector").val()=="")
      {
      	    alerta_modal("Se requiere sector");
      	return;
      }
         if($("#direccion_descripcion").val()=="")
      {
      	    alerta_modal("Se requiere direccion");
      	return;
      }

     json["cliente"]=$("#form_cliente").serialize();
     json["direccion"]=$("#form_direccion").serialize();
     json["detalle_pedido"]=$("#formulario_detalle_pedido").serialize();
    // console.log(json);
$("#button_generar_pedido").attr("disabled",true);
$("#button_generar_pedido").text("Generando....");

     $.post(base_url+"Registro_pedido/guardar_pedido",JSON.stringify(json),function(data){
              if(data["estado"]){


						  	toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "preventDuplicates": false,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							}
							toastr["success"]("Se registro Correctamente");


  							setTimeout(function(){ location.reload();}, 800);
              }else{
						toastr.options = {
									  "closeButton": true,
									  "debug": false,
									  "newestOnTop": false,
									  "progressBar": false,
									  "positionClass": "toast-bottom-right",
									  "preventDuplicates": false,
									  "showDuration": "300",
									  "hideDuration": "1000",
									  "timeOut": "5000",
									  "extendedTimeOut": "1000",
									  "showEasing": "swing",
									  "hideEasing": "linear",
									  "showMethod": "fadeIn",
									  "hideMethod": "fadeOut"
									}
									toastr["error"]("ERROR AL REGISTRAR");
$("#button_generar_pedido").attr("disabled",false);

$("#button_generar_pedido").text("Generar Pedido");
              }
     },"json");

	}


	/*function mostrar_direccion(resp)
	{

				$("#departamento").val(resp["id_departamento"]);
                $("#direccion_descripcion").val(resp["direccion_descripcion"]);
                $("#direccion_piso").val(resp["direccion_piso"]);
                $("#direccion_interior").val(resp["direccion_interior"]);
                $("#direccion_lote").val(resp["direccion_lote"]);
                $("#direccion_referencia").val(resp["direccion_referencia"]);
                $("#direccion_sector").val(resp["direccion_sector"]);
                $("#direccion_id").val(resp["direccion_id"]);
                $("#direccion_manzana").val(resp["direccion_manzana"]);

                $("#latitude_direccion").val(resp["direccion_latitud"]);
                $("#longitude_direccion").val(resp["direccion_longitud"]);



	}*/

	function mostrar_direccion1(resp)
	{
                  console.log(resp);
				$("#departamento").val(resp["id_departamento"]);
                $("#direccion_descripcion").val(resp["direccion_descripcion"]);
                $("#direccion_piso").val(resp["direccion_piso"]);
                $("#direccion_interior").val(resp["direccion_interior"]);
                $("#direccion_lote").val(resp["direccion_lote"]);
                $("#direccion_referencia").val(resp["direccion_referencia"]);
                $("#direccion_sector").val(resp["direccion_sector"]);
                $("#direccion_id").val(resp["direccion_id"]);
                $("#direccion_manzana").val(resp["direccion_manzana"]);

                $("#latitude_direccion").val(resp["direccion_latitud"]);
                $("#longitude_direccion").val(resp["direccion_longitud"]);

          $("#cargar_direcciones").modal("hide");

	}
	function seleccionar_deliverista(id,nombre)
	{
        $("#repartidor_id").val(id);
        $("#repartidor").val(nombre);
        $("#cargar_conductores").modal("hide");

	}


function mostrar_conductores()
{

	var html="";

$("#cargar_conductores").modal();
$.post(base_url+"Registro_pedido/cargar_pedido_deliverista",function(response){


	        if(response.length>0)
	        {
                   for (var i = 0; i < response.length; i++) {
                   	//response[i]
 var detalle=response[i]["direccion"];
                   	html+='<div id="headingCollapse'+response[i]["usu_id"]+'" class="card-header">';
					 	html+='<a  style="font-size: 15px;color: black;" data-toggle="collapse" href="#collapse'+response[i]["usu_id"]+'" aria-expanded="false" aria-controls="collapse11" class="card-title lead collapsed">';

									 	html+='<table >';
								 	html+='	  <thead >';
									    	html+=' <tr  >';
									    	html+='   <th style="width: 30px;">'+(i+1)+'</th>';
									       	html+='<th   style="width:250px !important;">'+response[i]["usu_nombre_completo"]+'</th>';
									      	html+=' <th   style="width: 130px;" >'+response[i]["usu_celular"]+'</th>';
									       	html+='<th   style="width: 140px;">16</th>';
									   	html+='    <th   style="width: 100px;">'+response[i]["usu_cantidad_max"]+'</th>';
									     	html+='  <th   style="width: 120px;">'+detalle.length+'</th>';
									     	var a="'"+response[i]["usu_id"]+"','"+response[i]["usu_nombre_completo"]+"'";

									     	if(detalle.length<parseInt(response[i]["usu_cantidad_max"])){
									     	html+='  <th   style="width: 10px;" ><i class="la la-check" onclick="seleccionar_deliverista('+a+')" style="color:blue"></i></th>';

									     	}else{

									     	html+='  <th   style="width: 10px;" ></th>';

									     	}


									     	html+='</tr>';
									  	html+=' </thead>';

								 	html+='	</table>';

				 	html+='	</a>';
				 	html+='</div>';

				 		html+='	<div id="collapse'+response[i]["usu_id"]+'" role="tabpanel" aria-labelledby="headingCollapse'+response[i]["usu_id"]+'" class="collapse">';
					html+='	<div class="card-content">';
					html+='		<div class="card-body">';

								html+='	<table >';
							html+='<tbody >';
                                 
                                     for (var j = 0; j < detalle.length; j++) {
                               html+='	<tr  style="border-bottom: 1px solid black;">';
								    html+='	<td   style="width:290px !important;">'+detalle[j]["cliente_nombres"]+" "+detalle[j]["cliente_apellido"]+'</td>';
									html+='	 <td   style="width: 130px;" >'+detalle[j]["cliente_telefono"]+'</td>';
									html+='	 <td   style="width: 140px;">'+detalle[j]["descripcion"]+'</td>';
								     html+='	  <td   style="width: 100px;">'+detalle[j]["pedido_direccion"]+'</td>';
									 html+='	   <td   style="width: 120px;color:blue;">'+detalle[j]["hora"]+':'+detalle[j]["minuto"]+'</td>';
									   html+='	 </tr>';							
								
                                     }
									html+='	  </tbody>';
								html+='		</table>';



						html+='	</div>';
					html+='		</div>';
					html+='		</div>';


                   }

                   $("#collapse_deliverista").empty().append(html);
	        }
	        else{
                 alerta_modal("No existe deliverista");

	        }

		},"json");


}


		/*		<div id="headingCollapse11" class="card-header">
					<a ondbclick="alert()" style="font-size: 15px;color: black;" data-toggle="collapse" href="#collapse11" aria-expanded="false" aria-controls="collapse11" class="card-title lead collapsed">

									<table >
									  <thead >
									    <tr >
									      <th style="width: 30px;">1</th>
									      <th   style="width:250px !important;">Juan Perez Rojas</th>
									      <th   style="width: 130px;" >95072321</th>
									      <th   style="width: 140px;">16</th>
									      <th   style="width: 100px;">4</th>
									      <th   style="width: 120px;">2</th>
									      <th   style="width: 10px;" ><i class="la la-trash " onclick="seleccionar_deliverista('1','Juan Perez Rojas')" style="color:red"></i></th>


									    </tr>
									  </thead>

									</table>

					</a>
				</div>
				<div id="collapse11" role="tabpanel" aria-labelledby="headingCollapse11" class="collapse">
					<div class="card-content">
						<div class="card-body">
						


                          
									<table >
									  <tbody >
									    <tr >
									     
									      <td   style="width:290px !important;">Lucy Rojas Lopez</td>
									      <td   style="width: 130px;" >95072321</td>
									      <td   style="width: 140px;">Tarapoto</td>
									      <td   style="width: 100px;">Jr. Sucre 258</td>
									      <td   style="width: 120px;">10:50</td>

									    </tr>
									      <tr >
									     
									      <td   style="width:290px !important;">Jimmy Jeiensto carbajal Sanchez</td>
									      <td   style="width: 130px;" >95072321</td>
									      <td   style="width: 140px;">Tarapoto</td>
									      <td   style="width: 100px;">Psje. Humberto Pinedo 258</td>
									      <td   style="width: 120px;">10:50</td>

									    </tr>
									  </tbody>

									</table>

						</div>
					</div>
				</div>*/



